// -*- mode:rust;coding:utf-8-unix; -*-

//! mod.rs
//!
//! # Examples
//!
//! ```
//! extern crate rusch;
//!
//! use rusch::RuSch;
//!
//! # fn main() -> rusch::Result<()> {
//! let _rusch = RuSch::new()?;
//! # Ok(())
//! # }
//! ```

//  Copyright 2017 hanepjiv
//  @author hanepjiv <hanepjiv@gmail.com>
//  @copyright The MIT License (MIT) / Apache License Version 2.0
//  @since 2018/07/05
//  @date 2024/12/14

// ////////////////////////////////////////////////////////////////////////////
// use  =======================================================================
// ----------------------------------------------------------------------------
use std::{
    fs::File,
    io::{BufReader, Read},
    path::Path,
};
// ----------------------------------------------------------------------------
use fraction::Fraction;
// ----------------------------------------------------------------------------
pub use self::error::{Error, Result};
// ----------------------------------------------------------------------------
pub(crate) use self::{
    atom::{
        Atom, AtomClass, Op, OpMixed, RcAtom, Reserved, Stacked, WeakAtom,
    },
    reader::{Parser, ReaderError},
    register::Reg,
    secd::{ARENA_LIMIT_MIN, SECD},
};
// mod  =======================================================================
mod error;
// ----------------------------------------------------------------------------
mod atom;
mod reader;
mod register;
mod secd;
// ////////////////////////////////////////////////////////////////////////////
// ============================================================================
/// struct `RuSch`
///
/// # Examples
///
/// ```
/// extern crate rusch;
///
/// use rusch::RuSch;
///
/// # fn main() -> rusch::Result<()> {
/// let _ = RuSch::new()?.entry()?;
/// # Ok(())
/// # }
/// ```
pub struct RuSch {
    /// secd
    secd: SECD,
    /// parser
    parser: Parser,
}
// ============================================================================
impl std::fmt::Debug for RuSch {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "RuSch {{ {:?}}}", self.secd)
    }
}
// ----------------------------------------------------------------------------
impl std::fmt::Display for RuSch {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "RuSch {{ {}}}", self.secd)
    }
}
// ============================================================================
impl RuSch {
    // constructor  ///////////////////////////////////////////////////////////
    // ========================================================================
    /// fn new
    ///
    /// # Errors
    ///
    /// - [`Error`]
    pub fn new() -> Result<Self> {
        #[cfg(debug_assertions)]
        log::debug!("{}: new", line!());
        Ok(Self {
            secd: SECD::new(ARENA_LIMIT_MIN)?,
            parser: Parser::new(),
        })
    }
    // pub  ///////////////////////////////////////////////////////////////////
    // ========================================================================
    /// fn clear
    ///
    /// # Errors
    ///
    /// - [`Error`]
    pub fn clear(&mut self) -> Result<&mut Self> {
        #[cfg(debug_assertions)]
        log::debug!("{}: clear", line!());
        let _ = self.secd.clear()?;
        Ok(self)
    }
    // ========================================================================
    /// fn gc
    ///
    /// # Errors
    ///
    /// - [`Error`]
    pub fn gc(&mut self) -> Result<&mut Self> {
        #[cfg(debug_assertions)]
        log::debug!("{}: gc", line!());
        let _ = self.secd.gc();
        Ok(self)
    }
    // ========================================================================
    /// fn entry
    ///
    /// # Errors
    ///
    /// - [`Error`]
    pub fn entry(&mut self) -> Result<&mut Self> {
        #[cfg(debug_assertions)]
        log::debug!("{}: entry", line!());
        let _ = self
            .secd
            .push(Reg::Stack, Fraction::new_neg(10u32, 3u32))?
            .push(Reg::Code, Op::Print)?
            .run()?
            .push(Reg::Stack, Atom::Str("RuSch".to_string()))?
            .push(Reg::Code, Op::Print)?
            .run()?
            .push(Reg::Stack, Atom::Symbol("rusch".to_string()))?
            .push(Reg::Code, Op::Print)?
            .run()?
            .push(Reg::Stack, true)?
            .push(Reg::Stack, false)?
            .push(Reg::Code, Op::Print)?
            .run()?
            .push(Reg::Code, Op::Print)?
            .push(Reg::Code, Op::MoveTop(Reg::Stack, Reg::Work))?
            .run()?
            .push(Reg::Code, Op::Print)?
            .push(Reg::Code, Op::MoveTop(Reg::Work, Reg::Stack))?
            .run()?
            .push(Reg::Stack, Atom::Nil)?
            .push(Reg::Stack, Fraction::new_neg(10u32, 3u32))?
            .push(Reg::Code, Op::Print)?
            .push(Reg::Code, Op::Head(Reg::Stack))?
            .push(Reg::Code, Op::Print)?
            .push(Reg::Code, Op::Cons(Reg::Stack))?
            .push(Reg::Code, Op::DumpSave(1usize, 0usize))?
            .push(Reg::Code, Op::Print)?
            .run()?
            .push(Reg::Stack, Atom::Symbol("display".to_string()))?
            .push(Reg::Code, Op::Print)?
            .push(Reg::Code, Op::Find)?
            .run()?
            .clear()?;
        Ok(self)
    }
    // ========================================================================
    /// fn `load_str`
    ///
    /// # Errors
    ///
    /// - [`Error::Reader`]
    pub fn load_str(&mut self, src: &str) -> Result<&mut Self> {
        #[cfg(debug_assertions)]
        log::debug!("{}: load_str", line!());
        let mut reader = BufReader::new(src.as_bytes());
        let _ = self.parser.parse(&mut self.secd, &mut reader)?;
        Ok(self)
    }
    // ------------------------------------------------------------------------
    /// fn `load_file`
    ///
    /// # Errors
    ///
    /// - [`Error::IOFsFileOpen`]
    /// - [`Error::Reader`]
    pub fn load_file(&mut self, path: &Path) -> Result<&mut Self> {
        #[cfg(debug_assertions)]
        log::debug!("{}: load_file", line!());
        log::info!("RuSch::load_file({path:?})");
        let mut reader = BufReader::new(
            File::open(path)
                .map_err(|_| Error::IOFsFileOpen(path.to_path_buf()))?,
        );
        let _ = self.parser.parse(&mut self.secd, &mut reader)?;
        Ok(self)
    }
    // ------------------------------------------------------------------------
    /// fn load
    ///
    /// # Errors
    ///
    /// - [`Error::Reader`]
    pub fn load<R>(&mut self, read: &mut impl Read) -> Result<&mut Self> {
        #[cfg(debug_assertions)]
        log::debug!("{}: load", line!());
        let _ = self.parser.parse(&mut self.secd, read)?;
        Ok(self)
    }
}
