// -*- mode:rust;coding:utf-8-unix; -*-

//! lib.rs

//  Copyright 2017 hanepjiv
//  @author hanepjiv <hanepjiv@gmail.com>
//  @copyright The MIT License (MIT) / Apache License Version 2.0
//  @since 2018/07/05
//  @date 2025/03/09

#![doc = include_str!(concat!(env!("CARGO_MANIFEST_DIR"), "/README.md"))]
// ////////////////////////////////////////////////////////////////////////////
// mod  =======================================================================
mod rusch;
// ////////////////////////////////////////////////////////////////////////////
// use  =======================================================================
use env_logger as _;
// ----------------------------------------------------------------------------
#[cfg(feature = "realize")]
use serde as _;
#[cfg(feature = "realize")]
use serde_derive as _;
// ----------------------------------------------------------------------------
pub use self::rusch::{Error, Result, RuSch};
// ////////////////////////////////////////////////////////////////////////////
/// const `MODULE_NAME`
pub const MODULE_NAME: &str = module_path!();
// ----------------------------------------------------------------------------
/// const `MODULE_VERSTR`
pub const MODULE_VERSTR: &str =
    concat!(module_path!(), " v", env!("CARGO_PKG_VERSION"));
