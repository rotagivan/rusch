// -*- mode:rust;coding:utf-8-unix; -*-

//! rusch/atom/mod.rs

//  Copyright 2017 hanepjiv
//  @author hanepjiv <hanepjiv@gmail.com>
//  @copyright The MIT License (MIT) / Apache License Version 2.0
//  @since 2018/07/05
//  @date 2025/03/09

// ////////////////////////////////////////////////////////////////////////////
// use  =======================================================================
use std::fmt::Display;
// ----------------------------------------------------------------------------
use fraction::Fraction;
// ----------------------------------------------------------------------------
use super::{Error, Reg, Result, SECD};
// ----------------------------------------------------------------------------
pub(crate) use self::{
    operator::{Op, OpMixed},
    rc::RcAtom,
    refcell::RefCellAtom,
    reserved::Reserved,
    stacked::Stacked,
    to_weak::ToWeak,
    weak::WeakAtom,
};
// mod  =======================================================================
mod cons;
mod operator;
mod rc;
mod refcell;
mod reserved;
mod stacked;
mod to_weak;
mod weak;
// ////////////////////////////////////////////////////////////////////////////
// ============================================================================
/// enum `AtomClass`
#[derive(Debug, Clone, Copy, PartialOrd, Ord, PartialEq, Eq, Hash)]
pub(crate) enum AtomClass {
    /// Nil
    #[allow(dead_code)]
    Nil,
    /// Undef
    Undef,
    /// Bool
    Bool,
    /// Number
    Number,
    /// Str
    Str,
    /// Symbol
    Symbol,
    /// Reserved
    Reserved,
    /// Op
    Op,
    /// Cons
    Cons,
    /// `MultiValue`
    #[allow(dead_code)]
    MultiValue,
    /// Continue
    Continue,
    /// Procedure
    Procedure,
}
// ////////////////////////////////////////////////////////////////////////////
// ============================================================================
/// enum Atom
#[derive(Debug, Clone)]
pub(crate) enum Atom {
    /// Nil
    #[allow(dead_code)]
    Nil,
    /// Undef
    Undef,
    /// Bool
    Bool(bool),
    /// Number
    Number(Fraction),
    /// Str
    Str(String),
    /// Symbol
    Symbol(String),
    /// Reserved
    Reserved(Reserved),
    /// Op
    Op(Op),
    /// Cons
    Cons { car: WeakAtom, cdr: WeakAtom },
    /// `MultiValue`
    #[allow(dead_code)]
    MultiValue { car: WeakAtom, cdr: WeakAtom },
    /// Continue
    Continue {
        stack: WeakAtom,
        env: WeakAtom,
        code: WeakAtom,
        dump: WeakAtom,
        work: WeakAtom,
        qqlv: usize,
    },
    /// Procedure
    Procedure {
        env: WeakAtom,
        formals: WeakAtom,
        body: WeakAtom,
    },
}
// ============================================================================
impl Display for Atom {
    #[allow(clippy::similar_names)]
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match *self {
            Self::Nil => write!(f, "nil"),
            Self::Op(ref x) => x.fmt(f),
            Self::Bool(x) => {
                if x {
                    write!(f, "#t")
                } else {
                    write!(f, "#f")
                }
            }
            Self::Number(ref x) => <Fraction as Display>::fmt(x, f),
            Self::Str(ref x) | Self::Symbol(ref x) => x.fmt(f),
            Self::Reserved(ref x) => x.fmt(f),
            Self::Cons { ref car, ref cdr } => {
                // write!(f, "({} . {})", car, cdr)
                let mut right = false;
                let mut space = "";
                let car_rc = car.upgrade().map_err(|_| std::fmt::Error)?;
                let car_atom = car_rc.as_ref().borrow();
                match *car_atom {
                    Self::Reserved(Reserved::Quote) => write!(f, "'")?,
                    Self::Reserved(Reserved::Quasiquote) => write!(f, "`")?,
                    Self::Reserved(Reserved::Unquote) => write!(f, ",")?,
                    Self::Reserved(Reserved::UnqSpl) => write!(f, ",@")?,
                    ref x => {
                        right = true;
                        space = " ";
                        write!(f, "({x}")?;
                    }
                }
                let mut cdr_ = cdr.clone();
                loop {
                    let cdr_rc =
                        cdr_.upgrade().map_err(|_| std::fmt::Error)?;
                    let cdr_atom = cdr_rc.as_ref().borrow();
                    match *cdr_atom {
                        Self::Nil => {
                            break;
                        }
                        Self::Cons {
                            car: ref car__,
                            cdr: ref cdr__,
                        } => {
                            write!(f, "{space}{car__}")?;
                            space = " ";
                            cdr_ = cdr__.clone();
                        }
                        ref x => {
                            write!(f, "{space}. {x})")?;
                            right = false;
                            break;
                        }
                    }
                }
                if right {
                    write!(f, ")")?;
                }
                Ok(())
            }
            Self::Continue {
                ref stack,
                ref env,
                ref code,
                ..
            } => write!(
                f,
                "Continue {{ stack: {stack}, env: {env}, code: {code} }}"
            ),
            Self::Procedure {
                ref formals,
                ref body,
                ..
            } => write!(f, "#<lambda {formals} {body}>"),
            _ => <Self as std::fmt::Debug>::fmt(self, f),
        }
    }
}
// ============================================================================
impl From<bool> for Atom {
    fn from(x: bool) -> Self {
        Self::Bool(x)
    }
}
// ----------------------------------------------------------------------------
impl From<Fraction> for Atom {
    fn from(x: Fraction) -> Self {
        Self::Number(x)
    }
}
// ----------------------------------------------------------------------------
impl From<Op> for Atom {
    fn from(x: Op) -> Self {
        Self::Op(x)
    }
}
// ----------------------------------------------------------------------------
impl From<Reserved> for Atom {
    fn from(x: Reserved) -> Self {
        Self::Reserved(x)
    }
}
// ============================================================================
impl Atom {
    // ========================================================================
    /// fn `new_cons`
    #[allow(clippy::similar_names)]
    pub(crate) const fn new_cons(car: WeakAtom, cdr: WeakAtom) -> Self {
        Self::Cons { car, cdr }
    }
    // ------------------------------------------------------------------------
    /// fn `new_continue`
    pub(crate) const fn new_continue(
        stack: WeakAtom,
        env: WeakAtom,
        code: WeakAtom,
        dump: WeakAtom,
        work: WeakAtom,
        qqlv: usize,
    ) -> Self {
        Self::Continue {
            stack,
            env,
            code,
            dump,
            work,
            qqlv,
        }
    }
    // ------------------------------------------------------------------------
    /// fn `new_procedure`
    pub(crate) const fn new_procedure(
        env: WeakAtom,
        formals: WeakAtom,
        body: WeakAtom,
    ) -> Self {
        Self::Procedure { env, formals, body }
    }
    // ========================================================================
    /// fn class
    pub(crate) const fn class(&self) -> AtomClass {
        match *self {
            Self::Nil => AtomClass::Nil,
            Self::Undef => AtomClass::Undef,
            Self::Op(_) => AtomClass::Op,
            Self::Bool(_) => AtomClass::Bool,
            Self::Number(_) => AtomClass::Number,
            Self::Str(_) => AtomClass::Str,
            Self::Symbol(_) => AtomClass::Symbol,
            Self::Reserved(_) => AtomClass::Reserved,
            Self::Cons { .. } => AtomClass::Cons,
            Self::MultiValue { .. } => AtomClass::MultiValue,
            Self::Continue { .. } => AtomClass::Continue,
            Self::Procedure { .. } => AtomClass::Procedure,
        }
    }
    // ========================================================================
    /// fn is
    pub(crate) fn is(&self, class: AtomClass) -> bool {
        #[cfg(debug_assertions)]
        log::debug!("{}: is", line!());
        self.class() == class
    }
}
// ============================================================================
impl ToWeak for Atom {
    fn to_weak(self, secd: &mut SECD) -> Result<WeakAtom> {
        #[cfg(debug_assertions)]
        log::debug!("{}: to_weak", line!());
        match self {
            Self::Nil => Ok(secd.new_nil()),
            Self::Undef => Ok(secd.new_undef()),
            Self::Op(x) => Ok(secd.new_op(x)),
            Self::Bool(x) => Ok(secd.new_bool(x)),
            Self::Number(x) => Ok(secd.new_num(x)),
            Self::Str(x) => Ok(secd.new_str(x)),
            Self::Symbol(x) => Ok(secd.new_symbol(x)),
            Self::Reserved(x) => Ok(secd.new_reserved(x)),
            Self::Cons { car, cdr } => Ok(secd.new_cons(car, cdr)),
            Self::MultiValue { .. } => unimplemented!(),
            Self::Continue {
                stack,
                env,
                code,
                dump,
                work,
                qqlv,
            } => Ok(secd.new_continue(stack, env, code, dump, work, qqlv)),
            Self::Procedure { env, formals, body } => {
                Ok(secd.new_procedure(env, formals, body))
            }
        }
    }
}
