// -*- mode:rust; coding:utf-8-unix; -*-

//! rc.rs

//  Copyright 2017 hanepjiv
//  @author hanepjiv <hanepjiv@gmail.com>
//  @copyright The MIT License (MIT) / Apache License Version 2.0
//  @since 2018/07/07
//  @date 2024/12/14

// ////////////////////////////////////////////////////////////////////////////
// use  =======================================================================
use std::{cell::RefCell, rc::Rc};
// ----------------------------------------------------------------------------
use log::debug;
// ----------------------------------------------------------------------------
use super::{Atom, AtomClass, RefCellAtom, Result, SECD, ToWeak, WeakAtom};
// ////////////////////////////////////////////////////////////////////////////
// ============================================================================
#[derive(Debug, Clone)]
pub(crate) struct RcAtom(Rc<RefCellAtom>);
// ============================================================================
impl std::fmt::Display for RcAtom {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.0.borrow())
    }
}
// ============================================================================
impl AsRef<RefCellAtom> for RcAtom {
    fn as_ref(&self) -> &RefCellAtom {
        self.0.as_ref()
    }
}
// ============================================================================
impl PartialEq for RcAtom {
    fn eq(&self, other: &Self) -> bool {
        Rc::<RefCellAtom>::ptr_eq(&self.0, &other.0)
    }
}
// ----------------------------------------------------------------------------
impl Eq for RcAtom {}
// ============================================================================
impl PartialEq<WeakAtom> for RcAtom {
    fn eq(&self, other: &WeakAtom) -> bool {
        self.eq(&(other
            .upgrade()
            .expect("<RcAtom as PartialEq<WeakAtom>>::eq: upgrade other")))
    }
}
// ============================================================================
impl RcAtom {
    // ========================================================================
    /// fn new
    pub(crate) fn new(atom: Atom) -> Self {
        Self(Rc::new(RefCell::new(atom)))
    }
    // ========================================================================
    /// fn `from_inner`
    pub(crate) const fn from_inner(inner: Rc<RefCellAtom>) -> Self {
        Self(inner)
    }
    // ========================================================================
    /// fn downgrade
    pub(crate) fn downgrade(&self) -> WeakAtom {
        WeakAtom::from_inner(Rc::<RefCellAtom>::downgrade(&self.0))
    }
    // ========================================================================
    /// fn `weak_count`
    pub(crate) fn weak_count(&self) -> usize {
        Rc::<RefCellAtom>::weak_count(&self.0)
    }
    // ========================================================================
    /// fn is
    pub(crate) fn is(&self, class: AtomClass) -> bool {
        let atom = self.0.as_ref().borrow();
        atom.is(class)
    }
}
// ============================================================================
impl ToWeak for RcAtom {
    fn to_weak(self, _secd: &mut SECD) -> Result<WeakAtom> {
        debug!("{} ({})", file!(), line!());
        Ok(Self::downgrade(&self))
    }
}
