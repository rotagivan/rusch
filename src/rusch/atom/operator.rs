// -*- mode:rust; coding:utf-8-unix; -*-

//! operator.rs

//  Copyright 2017 hanepjiv
//  @author hanepjiv <hanepjiv@gmail.com>
//  @copyright The MIT License (MIT) / Apache License Version 2.0
//  @since 2018/07/05
//  @date 2024/12/14

// ////////////////////////////////////////////////////////////////////////////
// use  =======================================================================
use super::{Reg, Reserved, Result, SECD, ToWeak, WeakAtom};
// ////////////////////////////////////////////////////////////////////////////
// ============================================================================
/// enum `OpMixed`
#[derive(Debug, Clone, Copy, PartialOrd, PartialEq, Eq, Ord, Hash)]
pub(crate) enum OpMixed {
    /// Begin
    Begin,
    /// Eval
    #[allow(dead_code)]
    Eval,
}
// ============================================================================
impl std::fmt::Display for OpMixed {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        <Self as std::fmt::Debug>::fmt(self, f)
    }
}
// ////////////////////////////////////////////////////////////////////////////
// ============================================================================
/// enum Op
#[allow(variant_size_differences)]
#[derive(Debug, Clone)]
pub(crate) enum Op {
    /// Push
    Push(Reg, WeakAtom),
    /// Pop
    Pop(Reg),
    /// Press
    Press(Reg),
    /// Swap
    #[allow(dead_code)]
    Swap(Reg),
    /// Head
    Head(Reg),
    /// Cons
    Cons(Reg),
    /// `XCons`
    XCons(Reg),
    /// Car
    Car(Reg),
    /// Cdr
    Cdr(Reg),
    /// List
    List(Reg),
    /// Unlist
    Unlist(Reg),
    /// `MoveTop`
    MoveTop(Reg, Reg),
    // ------------------------------------------------------------------------
    /// Jump
    Jump(usize),
    /// `JumpNil`
    JumpNil(usize),
    /// `JumpFalse`
    JumpFalse(usize),
    // ------------------------------------------------------------------------
    /// Print
    Print,
    // ------------------------------------------------------------------------
    /// Display
    Display,
    /// `DisplayLn`
    DisplayLn,
    /// `NewLine`
    NewLine,
    // ------------------------------------------------------------------------
    /// Find
    Find,
    /// `DefineImpl`
    DefineImpl,
    /// `SetxImpl`
    SetxImpl,
    // ------------------------------------------------------------------------
    /// `DumpSave`
    DumpSave(usize, usize),
    /// `DumpLoad`
    DumpLoad,
    /// Splice
    Splice,
    /// Eval
    Eval,
    /// `EvalRest`
    EvalRest,
    /// `QqlvInc`
    QqlvInc,
    /// `QqlvDec`
    QqlvDec,
    /// Call
    Call,
    /// Apply
    Apply,
    // ------------------------------------------------------------------------
    /// EQ ==
    EQ,
    /// NE !=
    NE,
    /// LT <
    LT,
    /// GT >
    GT,
    /// LE <=
    LE,
    /// GE >=
    GE,
    // ------------------------------------------------------------------------
    /// Mul
    Mul,
    /// Div
    Div,
    /// Add
    Add,
    /// Sub
    Sub,
    // ------------------------------------------------------------------------
    Reserved(Reserved),
    // ------------------------------------------------------------------------
    Mixed(OpMixed),
}
// ============================================================================
impl std::fmt::Display for Op {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        <Self as std::fmt::Debug>::fmt(self, f)
    }
}
// ============================================================================
impl From<OpMixed> for Op {
    fn from(x: OpMixed) -> Self {
        Self::Mixed(x)
    }
}
// ============================================================================
impl ToWeak for Op {
    fn to_weak(self, secd: &mut SECD) -> Result<WeakAtom> {
        #[cfg(debug_assertions)]
        log::debug!("{}: to_weak", line!());
        Ok(secd.new_op(self))
    }
}
