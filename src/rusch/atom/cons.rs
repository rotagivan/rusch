// -*- mode:rust;coding:utf-8-unix; -*-

//! cons.rs

//  Copyright 2017 hanepjiv
//  @author hanepjiv <hanepjiv@gmail.com>
//  @copyright The MIT License (MIT) / Apache License Version 2.0
//  @since 2018/07/07
//  @date 2024/12/05

// ////////////////////////////////////////////////////////////////////////////
// use  =======================================================================
use super::{Atom, Error, Result, WeakAtom};
// ////////////////////////////////////////////////////////////////////////////
// ============================================================================
#[allow(dead_code)]
impl Atom {
    // ========================================================================
    pub(crate) fn set_car(&mut self, x: WeakAtom) -> Result<&mut Self> {
        #[cfg(debug_assertions)]
        log::debug!("{}: set_car", line!());
        *self = match self {
            Self::Cons { cdr, .. } => Self::Cons {
                car: x,
                cdr: cdr.clone(),
            },
            _ => return Err(Error::NotCons(file!().to_string(), line!())),
        };
        Ok(self)
    }
    // ------------------------------------------------------------------------
    pub(crate) fn set_cdr(&mut self, x: WeakAtom) -> Result<&mut Self> {
        #[cfg(debug_assertions)]
        log::debug!("{}: set_cdr", line!());
        *self = match *self {
            Self::Cons { ref car, .. } => Self::Cons {
                car: car.clone(),
                cdr: x,
            },
            _ => return Err(Error::NotCons(file!().to_string(), line!())),
        };
        Ok(self)
    }
    // ========================================================================
    pub(crate) fn car(&self) -> Result<WeakAtom> {
        #[cfg(debug_assertions)]
        log::debug!("{}: car", line!());
        match *self {
            Self::Cons { ref car, .. } => Ok(car.clone()),
            _ => Err(Error::NotCons(file!().to_string(), line!())),
        }
    }
    // ------------------------------------------------------------------------
    pub(crate) fn cdr(&self) -> Result<WeakAtom> {
        #[cfg(debug_assertions)]
        log::debug!("{}: cdr", line!());
        match *self {
            Self::Cons { ref cdr, .. } => Ok(cdr.clone()),
            _ => Err(Error::NotCons(file!().to_string(), line!())),
        }
    }
    // ------------------------------------------------------------------------
    pub(crate) fn caar(&self) -> Result<WeakAtom> {
        #[cfg(debug_assertions)]
        log::debug!("{}: caar", line!());
        let rc = self.car()?.upgrade()?;
        let atom = rc.as_ref().borrow();
        match *atom {
            Self::Cons { ref car, .. } => Ok(car.clone()),
            _ => Err(Error::NotCons(file!().to_string(), line!())),
        }
    }
    // ------------------------------------------------------------------------
    pub(crate) fn cdar(&self) -> Result<WeakAtom> {
        #[cfg(debug_assertions)]
        log::debug!("{}: cdar", line!());
        let rc = self.car()?.upgrade()?;
        let atom = rc.as_ref().borrow();
        match *atom {
            Self::Cons { ref cdr, .. } => Ok(cdr.clone()),
            _ => Err(Error::NotCons(file!().to_string(), line!())),
        }
    }
    // ------------------------------------------------------------------------
    pub(crate) fn cadr(&self) -> Result<WeakAtom> {
        #[cfg(debug_assertions)]
        log::debug!("{}: cadr", line!());
        let rc = self.cdr()?.upgrade()?;
        let atom = rc.as_ref().borrow();
        match *atom {
            Self::Cons { ref car, .. } => Ok(car.clone()),
            _ => Err(Error::NotCons(file!().to_string(), line!())),
        }
    }
    // ------------------------------------------------------------------------
    pub(crate) fn cddr(&self) -> Result<WeakAtom> {
        #[cfg(debug_assertions)]
        log::debug!("{}: cddr", line!());
        let rc = self.cdr()?.upgrade()?;
        let atom = rc.as_ref().borrow();
        match *atom {
            Self::Cons { ref cdr, .. } => Ok(cdr.clone()),
            _ => Err(Error::NotCons(file!().to_string(), line!())),
        }
    }
    // ------------------------------------------------------------------------
    pub(crate) fn caaar(&self) -> Result<WeakAtom> {
        #[cfg(debug_assertions)]
        log::debug!("{}: caaa", line!());
        let rc = self.caar()?.upgrade()?;
        let atom = rc.as_ref().borrow();
        match *atom {
            Self::Cons { ref car, .. } => Ok(car.clone()),
            _ => Err(Error::NotCons(file!().to_string(), line!())),
        }
    }
    // ------------------------------------------------------------------------
    pub(crate) fn cdaar(&self) -> Result<WeakAtom> {
        #[cfg(debug_assertions)]
        log::debug!("{}: cdaar", line!());
        let rc = self.caar()?.upgrade()?;
        let atom = rc.as_ref().borrow();
        match *atom {
            Self::Cons { ref cdr, .. } => Ok(cdr.clone()),
            _ => Err(Error::NotCons(file!().to_string(), line!())),
        }
    }
    // ------------------------------------------------------------------------
    pub(crate) fn cadar(&self) -> Result<WeakAtom> {
        #[cfg(debug_assertions)]
        log::debug!("{}: cadar", line!());
        let rc = self.cdar()?.upgrade()?;
        let atom = rc.as_ref().borrow();
        match *atom {
            Self::Cons { ref car, .. } => Ok(car.clone()),
            _ => Err(Error::NotCons(file!().to_string(), line!())),
        }
    }
    // ------------------------------------------------------------------------
    pub(crate) fn cddar(&self) -> Result<WeakAtom> {
        #[cfg(debug_assertions)]
        log::debug!("{}: cddar", line!());
        let rc = self.cdar()?.upgrade()?;
        let atom = rc.as_ref().borrow();
        match *atom {
            Self::Cons { ref cdr, .. } => Ok(cdr.clone()),
            _ => Err(Error::NotCons(file!().to_string(), line!())),
        }
    }
    // ------------------------------------------------------------------------
    pub(crate) fn caadr(&self) -> Result<WeakAtom> {
        #[cfg(debug_assertions)]
        log::debug!("{}: caadr", line!());
        let rc = self.cadr()?.upgrade()?;
        let atom = rc.as_ref().borrow();
        match *atom {
            Self::Cons { ref car, .. } => Ok(car.clone()),
            _ => Err(Error::NotCons(file!().to_string(), line!())),
        }
    }
    // ------------------------------------------------------------------------
    pub(crate) fn cdadr(&self) -> Result<WeakAtom> {
        #[cfg(debug_assertions)]
        log::debug!("{}: cdadr", line!());
        let rc = self.cadr()?.upgrade()?;
        let atom = rc.as_ref().borrow();
        match *atom {
            Self::Cons { ref cdr, .. } => Ok(cdr.clone()),
            _ => Err(Error::NotCons(file!().to_string(), line!())),
        }
    }
    // ------------------------------------------------------------------------
    pub(crate) fn caddr(&self) -> Result<WeakAtom> {
        #[cfg(debug_assertions)]
        log::debug!("{}: caddr", line!());
        let rc = self.cddr()?.upgrade()?;
        let atom = rc.as_ref().borrow();
        match *atom {
            Self::Cons { ref car, .. } => Ok(car.clone()),
            _ => Err(Error::NotCons(file!().to_string(), line!())),
        }
    }
    // ------------------------------------------------------------------------
    pub(crate) fn cdddr(&self) -> Result<WeakAtom> {
        #[cfg(debug_assertions)]
        log::debug!("{}: cdddr", line!());
        let rc = self.cddr()?.upgrade()?;
        let atom = rc.as_ref().borrow();
        match *atom {
            Self::Cons { ref cdr, .. } => Ok(cdr.clone()),
            _ => Err(Error::NotCons(file!().to_string(), line!())),
        }
    }
}
