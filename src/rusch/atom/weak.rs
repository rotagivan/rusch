// -*- mode:rust;coding:utf-8-unix; -*-

//! weak.rs

//  Copyright 2017 hanepjiv
//  @author hanepjiv <hanepjiv@gmail.com>
//  @copyright The MIT License (MIT) / Apache License Version 2.0
//  @since 2018/07/07
//  @date 2025/03/09

// ////////////////////////////////////////////////////////////////////////////
// use  =======================================================================
use std::rc::{Rc, Weak};
// ----------------------------------------------------------------------------
use super::{AtomClass, Error, RcAtom, RefCellAtom, Result, SECD, ToWeak};
// ////////////////////////////////////////////////////////////////////////////
// ============================================================================
#[derive(Debug, Clone)]
pub(crate) struct WeakAtom(Weak<RefCellAtom>);
// ============================================================================
impl std::fmt::Display for WeakAtom {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.upgrade().map_err(|_| std::fmt::Error)?)
    }
}
// ============================================================================
impl PartialEq for WeakAtom {
    fn eq(&self, other: &Self) -> bool {
        #[cfg(debug_assertions)]
        log::debug!("{}: eq", line!());
        Rc::<RefCellAtom>::ptr_eq(
            &Weak::<RefCellAtom>::upgrade(&self.0)
                .expect("<WeakAtom as PartialEq<Self>>::eq: upgrade self"),
            &Weak::<RefCellAtom>::upgrade(&other.0)
                .expect("<WeakAtom as PartialEq<Self>>::eq: upgrade other"),
        )
    }
}
// ----------------------------------------------------------------------------
impl Eq for WeakAtom {}
// ============================================================================
impl PartialEq<RcAtom> for WeakAtom {
    fn eq(&self, other: &RcAtom) -> bool {
        #[cfg(debug_assertions)]
        log::debug!("{}: eq", line!());
        other.eq(&self
            .upgrade()
            .expect("<WeakAtom as PartialEq<RcAtom>>::eq: upgrade self"))
    }
}
// ============================================================================
impl WeakAtom {
    // ========================================================================
    /// fn `from_inner`
    pub(crate) const fn from_inner(inner: Weak<RefCellAtom>) -> Self {
        Self(inner)
    }
    // ========================================================================
    /// fn upgrade
    pub(crate) fn upgrade(&self) -> Result<RcAtom> {
        #[cfg(debug_assertions)]
        log::debug!("{}: upgrade", line!());
        Ok(RcAtom::from_inner(
            Weak::<RefCellAtom>::upgrade(&self.0).ok_or_else(|| {
                Error::WeakAtomUpgrade(file!().to_string(), line!())
            })?,
        ))
    }
    // ========================================================================
    /// fn at
    pub(crate) fn at(&self, offset: usize) -> Result<Self> {
        #[cfg(debug_assertions)]
        log::debug!("{}: at", line!());
        let mut ret = self.clone();
        for _ in 0usize..offset {
            let rc = ret.upgrade()?;
            let atom = rc.as_ref().borrow();
            ret = atom.cdr()?;
        }
        Ok(ret)
    }
    // ========================================================================
    /// fn is
    pub(crate) fn is(&self, class: AtomClass) -> Result<bool> {
        #[cfg(debug_assertions)]
        log::debug!("{}: is", line!());
        Ok(self.upgrade()?.is(class))
    }
}
// ============================================================================
impl ToWeak for WeakAtom {
    fn to_weak(self, _: &mut SECD) -> Result<WeakAtom> {
        #[cfg(debug_assertions)]
        log::debug!("{}: to_weak", line!());
        Ok(self)
    }
}
