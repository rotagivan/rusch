// -*- mode:rust; coding:utf-8-unix; -*-

//! stacked.rs

//  Copyright 2017 hanepjiv
//  @author hanepjiv <hanepjiv@gmail.com>
//  @copyright The MIT License (MIT) / Apache License Version 2.0
//  @since 2018/07/07
//  @date 2024/12/05

// ////////////////////////////////////////////////////////////////////////////
// use  =======================================================================
use super::{Reg, Result, SECD, ToWeak};
// ////////////////////////////////////////////////////////////////////////////
// ============================================================================
/// trait Stacked
pub(crate) trait Stacked: Sized + ToWeak {
    // ========================================================================
    /// fn push
    #[allow(dead_code)]
    fn push(self, secd: &mut SECD, reg: Reg) -> Result<&mut SECD> {
        #[cfg(debug_assertions)]
        log::debug!("{}: Stacked::push", line!());
        let weak = self.to_weak(secd)?;
        secd.push(reg, weak)
    }
}
// ============================================================================
impl<T> Stacked for T where T: Sized + ToWeak {}
