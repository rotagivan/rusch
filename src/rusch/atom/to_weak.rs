// -*- mode:rust; coding:utf-8-unix; -*-

//! `to_weak.rs`

//  Copyright 2017 hanepjiv
//  @author hanepjiv <hanepjiv@gmail.com>
//  @copyright The MIT License (MIT) / Apache License Version 2.0
//  @since 2018/07/13
//  @date 2024/12/14

// ////////////////////////////////////////////////////////////////////////////
// use  =======================================================================
use super::{Result, SECD, WeakAtom};
// ----------------------------------------------------------------------------
use fraction::Fraction;
// ////////////////////////////////////////////////////////////////////////////
// ============================================================================
/// trait `ToWeak`
pub(crate) trait ToWeak {
    // ========================================================================
    /// fn `to_weak`
    fn to_weak(self, _: &mut SECD) -> Result<WeakAtom>;
}
// ============================================================================
impl ToWeak for bool {
    fn to_weak(self, secd: &mut SECD) -> Result<WeakAtom> {
        #[cfg(debug_assertions)]
        log::debug!("{}: to_weak", line!());
        Ok(secd.new_bool(self))
    }
}
// ----------------------------------------------------------------------------
impl ToWeak for Fraction {
    fn to_weak(self, secd: &mut SECD) -> Result<WeakAtom> {
        #[cfg(debug_assertions)]
        log::debug!("{}: to_weak", line!());
        Ok(secd.new_num(self))
    }
}
