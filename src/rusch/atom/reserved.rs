// -*- mode:rust; coding:utf-8-unix; -*-

//! reserved.rs

//  Copyright 2017 hanepjiv
//  @author hanepjiv <hanepjiv@gmail.com>
//  @copyright The MIT License (MIT) / Apache License Version 2.0
//  @since 2018/07/13
//  @date 2024/12/14

// ////////////////////////////////////////////////////////////////////////////
// use  =======================================================================
use std::fmt::Display;
// ----------------------------------------------------------------------------
use super::{Result, SECD, ToWeak, WeakAtom};
// ////////////////////////////////////////////////////////////////////////////
// ============================================================================
/// enum Reserved
#[derive(Debug, Clone, Copy, PartialOrd, Ord, PartialEq, Eq, Hash)]
pub(crate) enum Reserved {
    /// Nil
    Nil,
    /// Dot
    Dot,
    /// Quote
    Quote,
    /// Quasiquote
    Quasiquote,
    /// `UnqSpl`
    UnqSpl,
    /// Unquote
    Unquote,
    /// Lambda
    Lambda,
    /// Let
    Let,
    /// Define
    Define,
    /// Setx
    Setx,
    /// Cond
    Cond,
    /// If
    If,
    /// Else
    Else,
}
// ============================================================================
impl From<Reserved> for &'static str {
    fn from(x: Reserved) -> Self {
        match x {
            Reserved::Nil => "nil",
            Reserved::Dot => ".",
            Reserved::Quote => "quote",
            Reserved::Quasiquote => "quasiquote",
            Reserved::UnqSpl => "unquote-splicing",
            Reserved::Unquote => "unquote",
            Reserved::Lambda => "lambda",
            Reserved::Let => "let",
            Reserved::Define => "define",
            Reserved::Setx => "set!",
            Reserved::Cond => "cond",
            Reserved::If => "if",
            Reserved::Else => "else",
        }
    }
}
// ============================================================================
impl Display for Reserved {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        <Self as Into<&'static str>>::into(*self).fmt(f)
    }
}
// ============================================================================
impl ToWeak for Reserved {
    fn to_weak(self, secd: &mut SECD) -> Result<WeakAtom> {
        #[cfg(debug_assertions)]
        log::debug!("{}: to_weak", line!());
        Ok(secd.new_reserved(self))
    }
}
