// -*- mode:rust; coding:utf-8-unix; -*-

//! token.rs

//  Copyright 2017 hanepjiv
//  @author hanepjiv <hanepjiv@gmail.com>
//  @copyright The MIT License (MIT) / Apache License Version 2.0
//  @since 2018/07/08
//  @date 2025/03/08

// ////////////////////////////////////////////////////////////////////////////
// use  =======================================================================
use fraction::Fraction;
// ----------------------------------------------------------------------------
use super::Reserved;
// ////////////////////////////////////////////////////////////////////////////
// ============================================================================
/// enum Token
#[derive(Debug, Clone, PartialEq, PartialOrd, Eq, Hash)]
pub(crate) enum Token {
    /// Ignore
    Ignore,
    /// EOF
    #[allow(clippy::upper_case_acronyms)]
    EOF,
    /// `CommentDatum`,
    CommentDatum,
    /// Nil
    #[allow(dead_code)]
    Nil,
    /// Bool
    Bool(bool),
    /// Number,
    Number(Fraction),
    /// Str
    Str(String),
    /// Symbol
    Symbol(String),
    /// Reserved
    Reserved(Reserved),
    /// `ParenL`
    ParenL,
    /// `ParenR`
    ParenR,
    /// `QuoteMark`,
    QuoteMark,
    /// `QuasiquoteMark`,
    QuasiquoteMark,
    /// `UnquoteMark`,
    UnquoteMark,
    /// `UnqSplMark`,
    UnqSplMark,
}
// ============================================================================
impl std::fmt::Display for Token {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match *self {
            Self::Number(ref x) => <Fraction as std::fmt::Display>::fmt(x, f),
            _ => <Self as std::fmt::Debug>::fmt(self, f),
        }
    }
}
// ////////////////////////////////////////////////////////////////////////////
// ============================================================================
#[cfg(test)]
mod tests {
    // use  ===================================================================
    use super::*;
    // ========================================================================
    #[test]
    const fn test_send() {
        const fn assert_send<T: Send>() {}
        assert_send::<Token>();
    }
    // ------------------------------------------------------------------------
    #[test]
    const fn test_sync() {
        const fn assert_sync<T: Sync>() {}
        assert_sync::<Token>();
    }
}
