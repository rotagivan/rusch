// -*- mode:rust;coding:utf-8-unix; -*-

//! error.rs

//  Copyright 2017 hanepjiv
//  @author hanepjiv <hanepjiv@gmail.com>
//  @copyright The MIT License (MIT) / Apache License Version 2.0
//  @since 2018/07/08
//  @date 2025/03/08

// ////////////////////////////////////////////////////////////////////////////
// use  =======================================================================
//use std::error::Error as StdError;
// ////////////////////////////////////////////////////////////////////////////
// ============================================================================
/// enum Error
#[allow(dead_code)]
#[derive(Debug)]
pub(crate) enum Error {
    /// `LexerUnknownByteEscape`
    LexerUnknownByteEscape(char),
    /// `LexerIllegal`
    LexerIllegal { id: u32, line: usize, column: usize },
    /// `LexerFail`
    LexerFail,
    /// `ParserInner`
    ParserInner(u32),
    /// `ParserFail`
    ParserFail { id: u32, line: usize, column: usize },
    /// `ParserParenRight`
    ParserParenRight { line: usize, column: usize },
    /// `ParserInvalidImproper`
    ParserInvalidImproper { line: usize, column: usize },

    /// `RuSch`
    RuSch { line: usize, column: usize },
    /// `OptionNone`
    OptionNone,
    /// `IOReadRead`
    IOReadRead,
}
// ============================================================================
impl std::fmt::Display for Error {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self, f)
    }
}
// ////////////////////////////////////////////////////////////////////////////
// ============================================================================
/// type Result
pub(crate) type Result<T> = std::result::Result<T, Error>;
// ////////////////////////////////////////////////////////////////////////////
// ============================================================================
#[cfg(test)]
mod tests {
    // use  ===================================================================
    use super::*;
    // ========================================================================
    #[test]
    const fn test_send() {
        const fn assert_send<T: Send>() {}
        assert_send::<Error>();
        assert_send::<Result<()>>();
    }
    // ------------------------------------------------------------------------
    #[test]
    const fn test_sync() {
        const fn assert_sync<T: Sync>() {}
        assert_sync::<Error>();
        assert_sync::<Result<()>>();
    }
}
