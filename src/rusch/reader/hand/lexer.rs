// -*- mode:rust;coding:utf-8-unix; -*-

//! lexer.rs

//  Copyright 2017 hanepjiv
//  @author hanepjiv <hanepjiv@gmail.com>
//  @copyright The MIT License (MIT) / Apache License Version 2.0
//  @since 2018/07/08
//  @date 2025/03/09

// ////////////////////////////////////////////////////////////////////////////
// use  =======================================================================
use std::io::Read;
// ----------------------------------------------------------------------------
use fraction::Fraction;
// ----------------------------------------------------------------------------
use super::{Error, Reserved, Result, Token};
// ////////////////////////////////////////////////////////////////////////////
// ============================================================================
/// const `WHITE_SPACE`
const WHITE_SPACE: &[u8] = b" \t\r\n";
// ----------------------------------------------------------------------------
/// const NUMBER
const NUMBER: &[u8] = b"1234567890";
// ----------------------------------------------------------------------------
/// const `SPECIAL_ALPHA_NUMBER`
const SPECIAL_ALPHA_NUMBER: &[u8] = b"-!$%&*+./:<=>?@^_~\
                                      abcdefghijklmnopqrstuvwxyz\
                                      ABCDEFGHIJKLMNOPQRSTUVWXYZ\
                                      1234567890";
// ----------------------------------------------------------------------------
/// const RESERVED
const RESERVED: &[Reserved] = &[
    Reserved::Nil,
    Reserved::Dot,
    Reserved::Quote,
    Reserved::Quasiquote,
    Reserved::UnqSpl,
    Reserved::Unquote,
    Reserved::Lambda,
    Reserved::Let,
    Reserved::Define,
    Reserved::Setx,
    Reserved::Cond,
    Reserved::If,
    Reserved::Else,
];
// ----------------------------------------------------------------------------
/// const KEYWORD
const KEYWORD: &[(&[u8], Token)] = &[
    (b"#true", Token::Bool(true)),
    (b"#false", Token::Bool(false)),
    (b"#t", Token::Bool(true)),
    (b"#f", Token::Bool(false)),
    (b",@", Token::UnqSplMark),
    (b"(", Token::ParenL),
    (b")", Token::ParenR),
    (b"\'", Token::QuoteMark),
    (b"`", Token::QuasiquoteMark),
    (b",", Token::UnquoteMark),
];
// ////////////////////////////////////////////////////////////////////////////
// ============================================================================
/// enum Rule
#[derive(Debug, Clone, PartialOrd, Ord, PartialEq, Eq, Hash)]
enum Rule {
    /// EOF,
    #[allow(clippy::upper_case_acronyms)]
    EOF,
    /// `WhiteSpace`,
    WhiteSpace,
    /// `CommentLine`,
    CommentLine,
    /// `CommentBlock`,
    CommentBlock,
    /// `CommentDatum`,
    CommentDatum,
    /// Keyword
    Keyword,
    /// Symbol
    Symbol,
    /// `StringLiteral`
    StringLiteral,
    /// Any
    Any(Vec<Rule>),
}
// ============================================================================
impl Rule {
    // ========================================================================
    fn apply(&self, lexer: &mut Lexer, read: &mut impl Read) -> Result<Token> {
        #[cfg(debug_assertions)]
        log::debug!("{}: apply", line!());
        match *self {
            Self::EOF => lexer.eof(read),
            Self::WhiteSpace => lexer.white_space(read),
            Self::CommentLine => lexer.comment_line(read),
            Self::CommentBlock => lexer.comment_block(read),
            Self::CommentDatum => lexer.comment_datum(read),
            Self::Keyword => lexer.keyword(read),
            Self::Symbol => lexer.symbol(read),
            Self::StringLiteral => lexer.string_literal(read),
            Self::Any(ref rules) => {
                for rule in rules {
                    match rule.apply(lexer, read) {
                        Err(Error::LexerFail) => {}
                        x @ (Err(_) | Ok(_)) => {
                            return x;
                        }
                    }
                }
                Err(lexer.illegal(line!()))
            }
        }
    }
}
// ////////////////////////////////////////////////////////////////////////////
// ============================================================================
/// enum Check
#[derive(Debug, Clone, Copy, PartialOrd, Ord, PartialEq, Eq, Hash)]
enum Check {
    /// Char
    Char(u8),
    /// `FalseChar`
    FalseChar,
    /// Str
    Str,
    /// `FalseStr`
    FalseStr,
}
// ////////////////////////////////////////////////////////////////////////////
// ============================================================================
/// struct Lexer
pub(crate) struct Lexer {
    /// line
    line: usize,
    /// column
    column: usize,
    /// cache
    cache: Vec<u8>,
}
// ============================================================================
impl Lexer {
    // ========================================================================
    /// fn new
    pub(crate) fn new() -> Self {
        #[cfg(debug_assertions)]
        log::debug!("{}: new", line!());
        Self {
            line: 1usize,
            column: 0usize,
            cache: Vec::default(),
        }
    }
    // ========================================================================
    /// fn clear
    pub(crate) fn clear(&mut self) {
        #[cfg(debug_assertions)]
        log::debug!("{}: clear", line!());
        self.line = 1usize;
        self.column = 0usize;
        self.cache.clear();
        self.cache.shrink_to_fit();
    }
    // ========================================================================
    /// fn `line_column`
    pub(crate) const fn line_column(&self) -> (usize, usize) {
        (self.line, self.column)
    }
    // ------------------------------------------------------------------------
    /// fn `line_column_forward`
    const fn line_column_forward(&mut self, c: u8) {
        if b'\n' == c {
            self.line += 1;
            self.column = 0;
        } else {
            self.column += 1;
        }
    }
    // ------------------------------------------------------------------------
    /// fn `line_column_back`
    const fn line_column_back(&mut self, c: u8) {
        if b'\n' == c {
            self.line -= 1;
            self.column = usize::MAX / 2usize;
        } else {
            self.column -= 1;
        }
    }
    // ========================================================================
    /// fn push
    fn push(&mut self, c: u8) {
        #[cfg(debug_assertions)]
        log::debug!("{}: push", line!());
        self.line_column_back(c);
        self.cache.push(c);
    }
    // ------------------------------------------------------------------------
    /// fn pop
    fn pop(&mut self, read: &mut (impl Read + ?Sized)) -> Result<Option<u8>> {
        #[cfg(debug_assertions)]
        log::debug!("{}: pop", line!());
        if self.cache.is_empty() {
            let mut buf = [0u8; 1];
            let len = read.read(&mut buf).map_err(|_| Error::IOReadRead)?;
            if 0usize == len {
                Ok(None)
            } else {
                let c = buf[0];
                self.line_column_forward(c);
                Ok(Some(c))
            }
        } else {
            let c = self.cache.pop().unwrap();
            self.line_column_forward(c);
            Ok(Some(c))
        }
    }
    // ========================================================================
    /// fn check
    fn check(&mut self, read: &mut impl Read, x: u8) -> Result<Option<Check>> {
        #[cfg(debug_assertions)]
        log::debug!("{}: check", line!());

        (self.pop(read)?).map_or(Ok(None), |c| {
            if c == x {
                Ok(Some(Check::Char(c)))
            } else {
                self.push(c);
                Ok(Some(Check::FalseChar))
            }
        })
    }
    // ------------------------------------------------------------------------
    /// fn `check_str`
    fn check_str(
        &mut self,
        read: &mut impl Read,
        xs: &[u8],
    ) -> Result<Option<Check>> {
        #[cfg(debug_assertions)]
        log::debug!("{}: check_str", line!());
        for (idx, c) in xs.iter().enumerate() {
            if let Some(check) = self.check(read, *c)? {
                if let Check::Char(_) = check {
                    continue;
                }
                for i in 0..idx {
                    self.push(xs[idx - 1 - i]);
                }
                return Ok(Some(Check::FalseStr));
            }
            for i in 0..idx {
                self.push(xs[idx - 1 - i]);
            }
            return Ok(None);
        }
        Ok(Some(Check::Str))
    }
    // ------------------------------------------------------------------------
    /// fn `check_contains`
    fn check_contains(
        &mut self,
        read: &mut impl Read,
        xs: &[u8],
    ) -> Result<Option<Check>> {
        #[cfg(debug_assertions)]
        log::debug!("{}: check_contains", line!());
        if let Some(c) = self.pop(read)? {
            if xs.contains(&c) {
                return Ok(Some(Check::Char(c)));
            }
            self.push(c);
            Ok(Some(Check::FalseChar))
        } else {
            Ok(None)
        }
    }
    // ========================================================================
    /// fn illegal
    const fn illegal(&self, id: u32) -> Error {
        Error::LexerIllegal {
            id,
            line: self.line,
            column: self.column,
        }
    }
    // ========================================================================
    /// fn token
    pub(crate) fn token(&mut self, read: &mut impl Read) -> Result<Token> {
        #[cfg(debug_assertions)]
        log::debug!("{}: token", line!());
        loop {
            match Rule::Any(vec![
                Rule::EOF,
                Rule::WhiteSpace,
                Rule::CommentLine,
                Rule::CommentBlock,
                Rule::CommentDatum,
                Rule::Keyword,
                Rule::Symbol,
                Rule::StringLiteral,
            ])
            .apply(self, read)
            {
                Ok(Token::Ignore) => {}
                x => {
                    return x;
                }
            }
        }
    }
    // =======================================1=================================
    /// fn eof
    fn eof(&mut self, read: &mut (impl Read + ?Sized)) -> Result<Token> {
        #[cfg(debug_assertions)]
        log::debug!("{}: eof", line!());

        (self.pop(read)?).map_or(Ok(Token::EOF), |c| {
            self.push(c);
            Err(Error::LexerFail)
        })
    }
    // ========================================================================
    /// fn `white_space`
    fn white_space(&mut self, read: &mut impl Read) -> Result<Token> {
        #[cfg(debug_assertions)]
        log::debug!("{}: white_space", line!());
        if self
            .check_contains(read, WHITE_SPACE)?
            .ok_or_else(|| self.illegal(line!()))?
            == Check::FalseChar
        {
            return Err(Error::LexerFail);
        }
        loop {
            let Some(check) = self.check_contains(read, WHITE_SPACE)? else {
                return Ok(Token::Ignore);
            };
            let Check::Char(_) = check else {
                return Ok(Token::Ignore);
            };
        }
    }
    // ========================================================================
    /// fn `comment_line`
    fn comment_line(&mut self, read: &mut impl Read) -> Result<Token> {
        #[cfg(debug_assertions)]
        log::debug!("{}: comment_line", line!());
        if self
            .check(read, b';')?
            .ok_or_else(|| self.illegal(line!()))?
            == Check::FalseChar
        {
            return Err(Error::LexerFail);
        }
        loop {
            if b'\n' == self.pop(read)?.ok_or_else(|| self.illegal(line!()))? {
                return Ok(Token::Ignore);
            }
        }
    }
    // ========================================================================
    /// fn `comment_block`
    fn comment_block(&mut self, read: &mut impl Read) -> Result<Token> {
        #[cfg(debug_assertions)]
        log::debug!("{}: comment_back", line!());
        if self
            .check_str(read, b"#|")?
            .ok_or_else(|| self.illegal(line!()))?
            == Check::FalseStr
        {
            return Err(Error::LexerFail);
        }
        loop {
            if b'|' == self.pop(read)?.ok_or_else(|| self.illegal(line!()))?
                && b'#'
                    == self.pop(read)?.ok_or_else(|| self.illegal(line!()))?
            {
                return Ok(Token::Ignore);
            }
        }
    }
    // ========================================================================
    /// fn `comment_datum`
    fn comment_datum(&mut self, read: &mut impl Read) -> Result<Token> {
        #[cfg(debug_assertions)]
        log::debug!("{}: comment_datum", line!());
        let Check::Str = self
            .check_str(read, b"#;")?
            .ok_or_else(|| self.illegal(line!()))?
        else {
            return Err(Error::LexerFail);
        };
        Ok(Token::CommentDatum)
    }
    // ========================================================================
    /// fn keyword
    fn keyword(&mut self, read: &mut impl Read) -> Result<Token> {
        #[cfg(debug_assertions)]
        log::debug!("{}: keyword", line!());
        for (xs, token) in KEYWORD {
            if self.check_str(read, xs)? == Some(Check::Str) {
                return Ok(token.clone());
            }
        }
        Err(Error::LexerFail)
    }
    // ========================================================================
    /// fn symbol
    fn symbol(&mut self, read: &mut impl Read) -> Result<Token> {
        #[cfg(debug_assertions)]
        log::debug!("{}: symbol", line!());
        let mut ret = String::default();
        loop {
            if let Some(Check::Char(c)) =
                self.check_contains(read, SPECIAL_ALPHA_NUMBER)?
            {
                ret.push(char::from(c));
                continue;
            } else if ret.is_empty() {
                return Err(Error::LexerFail);
            } else if let Some(x) = Self::reserved(ret.as_bytes()) {
                return Ok(x);
            } else if let Some(x) = Self::fraction(ret.as_bytes()) {
                return Ok(Token::Number(x));
            }
            return Ok(Token::Symbol(ret));
        }
    }
    // ------------------------------------------------------------------------
    /// fn reserved
    fn reserved(src: &[u8]) -> Option<Token> {
        #[cfg(debug_assertions)]
        log::debug!("{}: reserved", line!());
        let len = src.len();
        'outer: for x in RESERVED {
            let xs = <Reserved as Into<&'static str>>::into(*x);
            if xs.len() != len {
                continue 'outer;
            }
            for (i, x) in xs.as_bytes().iter().enumerate() {
                if *x != src[i] {
                    continue 'outer;
                }
            }
            return Some(Token::Reserved(*x));
        }
        None
    }
    // ------------------------------------------------------------------------
    /// `u8_to_digit`
    fn u8_to_digit(c: u8) -> usize {
        #[cfg(debug_assertions)]
        log::debug!("{}: u8_to_digit", line!());
        #[cfg(debug_assertions)]
        assert!(NUMBER.contains(&c));
        char::from(c).to_digit(10).unwrap() as usize
    }
    // ------------------------------------------------------------------------
    /// fn `new_big_fraction`
    fn new_big_fraction(
        sign: Option<u8>,
        num: usize,
        denum: usize,
    ) -> Fraction {
        #[cfg(debug_assertions)]
        log::debug!("{}: new_big_fraction", line!());
        if sign == Some(b'-') {
            Fraction::new_neg(num as u64, denum as u64)
        } else {
            Fraction::new(num as u64, denum as u64)
        }
    }
    // ------------------------------------------------------------------------
    /// fn fraction
    fn fraction(src: &[u8]) -> Option<Fraction> {
        #[cfg(debug_assertions)]
        log::debug!("{}: fraction", line!());
        let len = src.len();
        let mut idx = 0usize;

        let mut c = src[idx];

        let mut sign = None;
        if b"+-".contains(&c) {
            sign = Some(c);
            idx += 1usize;
        }

        if len <= idx {
            return None;
        }

        let mut num = 0usize;
        while idx < len {
            c = src[idx];
            idx += 1usize;
            if !NUMBER.contains(&c) {
                if b'.' != c {
                    return None;
                }
                break;
            }
            num *= 10usize;
            num += Self::u8_to_digit(c);
        }

        let mut ret = Self::new_big_fraction(sign, num, 1usize);

        if len <= idx {
            return Some(ret);
        }

        if b'.' == c {
            c = src[idx];
            idx += 1usize;
            if NUMBER.contains(&c) {
                let mut denum = Self::u8_to_digit(c);
                let mut factor = 10usize;
                while idx < len {
                    c = src[idx];
                    idx += 1usize;
                    if !NUMBER.contains(&c) {
                        return None;
                    }
                    denum *= 10usize;
                    denum += Self::u8_to_digit(c);
                    factor *= 10usize;
                }
                ret += Self::new_big_fraction(sign, denum, factor);
            }
        }

        Some(ret)
    }
    // ========================================================================
    /// fn `string_literal`
    fn string_literal(&mut self, read: &mut impl Read) -> Result<Token> {
        #[cfg(debug_assertions)]
        log::debug!("{}: string_literal", line!());
        if self
            .check(read, b'"')?
            .ok_or_else(|| self.illegal(line!()))?
            == Check::FalseChar
        {
            return Err(Error::LexerFail);
        }
        let mut ret = String::default();
        loop {
            let c = self.pop(read)?.ok_or_else(|| self.illegal(line!()))?;
            match c {
                b'"' => {
                    break;
                }
                b'\\' => {
                    let c = self
                        .pop(read)?
                        .ok_or_else(|| self.illegal(line!()))?;
                    match c {
                        b'\\' => ret.push('\\'),
                        b'"' => ret.push('"'),
                        b'n' => ret.push('\n'),
                        b'r' => ret.push('\r'),
                        b't' => ret.push('\t'),
                        b'0' => ret.push('\0'),
                        x => {
                            return Err(Error::LexerUnknownByteEscape(
                                x.into(),
                            ));
                        }
                    }
                }
                // b'/' => {}
                x => {
                    ret.push(x.into());
                }
            }
        }
        Ok(Token::Str(ret))
    }
}
// ////////////////////////////////////////////////////////////////////////////
// ============================================================================
#[cfg(test)]
mod tests {
    // use  ===================================================================
    use std::io::Cursor;
    // ------------------------------------------------------------------------
    use fraction::Fraction;
    // ------------------------------------------------------------------------
    use super::{
        super::{Error, Lexer, Reserved, Token},
        RESERVED,
    };
    // ========================================================================
    #[test]
    fn eof() {
        assert_eq!(
            Lexer::new().eof(&mut Cursor::<_>::new(b"")).unwrap(),
            Token::EOF
        );
    }
    // ------------------------------------------------------------------------
    #[test]
    fn eof_fail() {
        match Lexer::new().eof(&mut Cursor::<_>::new(b"rusch")) {
            Err(Error::LexerFail) => {}
            Err(x) => panic!("{x}"),
            _ => {
                panic!()
            }
        }
    }
    // ========================================================================
    #[test]
    fn white_space_0() {
        assert_eq!(
            Lexer::new()
                .white_space(&mut Cursor::<_>::new(b" "))
                .unwrap(),
            Token::Ignore
        );
    }
    // ------------------------------------------------------------------------
    #[test]
    fn white_space_1() {
        assert_eq!(
            Lexer::new()
                .white_space(&mut Cursor::<_>::new(b"  "))
                .unwrap(),
            Token::Ignore
        );
    }
    // ------------------------------------------------------------------------
    #[test]
    fn white_space_2() {
        assert_eq!(
            Lexer::new()
                .white_space(&mut Cursor::<_>::new(b"\t"))
                .unwrap(),
            Token::Ignore
        );
    }
    // ------------------------------------------------------------------------
    #[test]
    fn white_space_3() {
        assert_eq!(
            Lexer::new()
                .white_space(&mut Cursor::<_>::new(b"\t\t"))
                .unwrap(),
            Token::Ignore
        );
    }
    // ------------------------------------------------------------------------
    #[test]
    fn white_space_4() {
        assert_eq!(
            Lexer::new()
                .white_space(&mut Cursor::<_>::new(b"\n"))
                .unwrap(),
            Token::Ignore
        );
    }
    // ------------------------------------------------------------------------
    #[test]
    fn white_space_5() {
        assert_eq!(
            Lexer::new()
                .white_space(&mut Cursor::<_>::new(b"\n\n"))
                .unwrap(),
            Token::Ignore
        );
    }
    // ------------------------------------------------------------------------
    #[test]
    fn white_space_6() {
        assert_eq!(
            Lexer::new()
                .white_space(&mut Cursor::<_>::new(b"\r\n"))
                .unwrap(),
            Token::Ignore
        );
    }
    // ------------------------------------------------------------------------
    #[test]
    fn white_space_7() {
        assert_eq!(
            Lexer::new()
                .white_space(&mut Cursor::<_>::new(b"\r\n\r\n"))
                .unwrap(),
            Token::Ignore
        );
    }
    // ------------------------------------------------------------------------
    #[test]
    fn white_space_8() {
        assert_eq!(
            Lexer::new()
                .white_space(&mut Cursor::<_>::new(b" \t\r\n"))
                .unwrap(),
            Token::Ignore
        );
    }
    // ------------------------------------------------------------------------
    #[test]
    fn white_space_fail() {
        match Lexer::new().white_space(&mut Cursor::<_>::new(b"rusch")) {
            Err(Error::LexerFail) => {}
            Err(x) => {
                panic!("{x}")
            }
            _ => {
                panic!()
            }
        }
    }
    // ========================================================================
    #[test]
    fn comment_line() {
        assert_eq!(
            Lexer::new()
                .comment_line(&mut Cursor::<_>::new(b"; ==\n"))
                .unwrap(),
            Token::Ignore
        );
    }
    // ------------------------------------------------------------------------
    #[test]
    fn comment_line_fail() {
        match Lexer::new().comment_line(&mut Cursor::<_>::new(b"rusch")) {
            Err(Error::LexerFail) => {}
            Err(x) => {
                panic!("{x}")
            }
            _ => {
                panic!()
            }
        }
    }
    // ========================================================================
    #[test]
    fn comment_block_0() {
        assert_eq!(
            Lexer::new()
                .comment_block(&mut Cursor::<_>::new(b"#||#"))
                .unwrap(),
            Token::Ignore
        );
    }
    // ------------------------------------------------------------------------
    #[test]
    fn comment_block_1() {
        assert_eq!(
            Lexer::new()
                .comment_block(&mut Cursor::<_>::new(b"#| |#"))
                .unwrap(),
            Token::Ignore
        );
    }
    // ------------------------------------------------------------------------
    #[test]
    fn comment_block_2() {
        assert_eq!(
            Lexer::new()
                .comment_block(&mut Cursor::<_>::new(b"#|=|#"))
                .unwrap(),
            Token::Ignore
        );
    }
    // ------------------------------------------------------------------------
    #[test]
    fn comment_block_3() {
        assert_eq!(
            Lexer::new()
                .comment_block(&mut Cursor::<_>::new(b"#|\n|#"))
                .unwrap(),
            Token::Ignore
        );
    }
    // ------------------------------------------------------------------------
    #[test]
    fn comment_block_fail_0() {
        match Lexer::new().comment_block(&mut Cursor::<_>::new(b"rusch")) {
            Err(Error::LexerFail) => {}
            Err(x) => {
                panic!("{x}")
            }
            _ => {
                panic!()
            }
        }
    }
    // ------------------------------------------------------------------------
    #[test]
    fn comment_block_fail_1() {
        match Lexer::new().comment_block(&mut Cursor::<_>::new(b"#|")) {
            Err(Error::LexerIllegal { .. }) => {}
            Err(x) => {
                panic!("{x}")
            }
            _ => {
                panic!()
            }
        }
    }
    // ========================================================================
    #[test]
    fn comment_datum() {
        assert_eq!(
            Lexer::new()
                .comment_datum(&mut Cursor::<_>::new(b"#;"))
                .unwrap(),
            Token::CommentDatum
        );
    }
    // ------------------------------------------------------------------------
    #[test]
    fn comment_datum_fail() {
        match Lexer::new().comment_datum(&mut Cursor::<_>::new(b"rusch")) {
            Err(Error::LexerFail) => {}
            Err(x) => {
                panic!("{x}")
            }
            _ => {
                panic!()
            }
        }
    }
    // ========================================================================
    #[test]
    fn keyword_true_t() {
        assert_eq!(
            Lexer::new().keyword(&mut Cursor::<_>::new(b"#t")).unwrap(),
            Token::Bool(true)
        );
    }
    // ------------------------------------------------------------------------
    #[test]
    fn keyword_true_true() {
        assert_eq!(
            Lexer::new()
                .keyword(&mut Cursor::<_>::new(b"#true"))
                .unwrap(),
            Token::Bool(true)
        );
    }
    // ------------------------------------------------------------------------
    #[test]
    fn keyword_false_f() {
        assert_eq!(
            Lexer::new().keyword(&mut Cursor::<_>::new(b"#f")).unwrap(),
            Token::Bool(false)
        );
    }
    // ------------------------------------------------------------------------
    #[test]
    fn keyword_false_false() {
        assert_eq!(
            Lexer::new()
                .keyword(&mut Cursor::<_>::new(b"#false"))
                .unwrap(),
            Token::Bool(false)
        );
    }
    // ------------------------------------------------------------------------
    #[test]
    fn keyword_unqqpl_mark() {
        assert_eq!(
            Lexer::new().keyword(&mut Cursor::<_>::new(b",@")).unwrap(),
            Token::UnqSplMark
        );
    }
    // ------------------------------------------------------------------------
    #[test]
    fn keyword_paren_l() {
        assert_eq!(
            Lexer::new().keyword(&mut Cursor::<_>::new(b"(")).unwrap(),
            Token::ParenL
        );
    }
    // ------------------------------------------------------------------------
    #[test]
    fn keyword_paren_r() {
        assert_eq!(
            Lexer::new().keyword(&mut Cursor::<_>::new(b")")).unwrap(),
            Token::ParenR
        );
    }
    // ------------------------------------------------------------------------
    #[test]
    fn keyword_quote() {
        assert_eq!(
            Lexer::new().keyword(&mut Cursor::<_>::new(b"\'")).unwrap(),
            Token::QuoteMark
        );
    }
    // ------------------------------------------------------------------------
    #[test]
    fn keyword_quasiquote() {
        assert_eq!(
            Lexer::new().keyword(&mut Cursor::<_>::new(b"`")).unwrap(),
            Token::QuasiquoteMark
        );
    }
    // ------------------------------------------------------------------------
    #[test]
    fn keyword_unquote() {
        assert_eq!(
            Lexer::new().keyword(&mut Cursor::<_>::new(b",")).unwrap(),
            Token::UnquoteMark
        );
    }
    // ========================================================================
    #[test]
    fn symbol_0() {
        assert_eq!(
            Lexer::new().symbol(&mut Cursor::<_>::new(b"a")).unwrap(),
            Token::Symbol("a".to_string())
        );
    }
    // ------------------------------------------------------------------------
    #[test]
    fn symbol_1() {
        assert_eq!(
            Lexer::new().symbol(&mut Cursor::<_>::new(b"0a")).unwrap(),
            Token::Symbol("0a".to_string())
        );
    }
    // ------------------------------------------------------------------------
    #[test]
    fn symbol_reserved() {
        for x in RESERVED {
            assert_eq!(
                Lexer::new()
                    .symbol(
                        &mut <Reserved as Into<&'static str>>::into(*x)
                            .as_bytes()
                    )
                    .unwrap(),
                Token::Reserved(*x)
            );
        }
    }
    // ------------------------------------------------------------------------
    #[test]
    fn symbol_number_0() {
        assert_eq!(
            Lexer::new().symbol(&mut Cursor::<_>::new(b"0")).unwrap(),
            Token::Number(Fraction::new(0u64, 1u64))
        );
    }
    // ------------------------------------------------------------------------
    #[test]
    fn symbol_number_1() {
        assert_eq!(
            Lexer::new().symbol(&mut Cursor::<_>::new(b"+0")).unwrap(),
            Token::Number(Fraction::new(0u64, 1u64))
        );
    }
    // ------------------------------------------------------------------------
    #[test]
    fn symbol_number_2() {
        assert_eq!(
            Lexer::new().symbol(&mut Cursor::<_>::new(b"-0")).unwrap(),
            Token::Number(Fraction::new_neg(0u64, 1u64))
        );
    }
    // ------------------------------------------------------------------------
    #[test]
    fn symbol_number_3() {
        assert_eq!(
            Lexer::new().symbol(&mut Cursor::<_>::new(b"0.")).unwrap(),
            Token::Number(Fraction::new(0u64, 1u64))
        );
    }
    // ------------------------------------------------------------------------
    #[test]
    fn symbol_number_4() {
        assert_eq!(
            Lexer::new().symbol(&mut Cursor::<_>::new(b"+0.1")).unwrap(),
            Token::Number(Fraction::new(1u64, 10u64))
        );
    }
    // ------------------------------------------------------------------------
    #[test]
    fn symbol_number_5() {
        assert_eq!(
            Lexer::new().symbol(&mut Cursor::<_>::new(b"-0.1")).unwrap(),
            Token::Number(Fraction::new_neg(1u64, 10u64))
        );
    }
    // ------------------------------------------------------------------------
    #[test]
    fn symbol_number_6() {
        assert_eq!(
            Lexer::new().symbol(&mut Cursor::<_>::new(b".1")).unwrap(),
            Token::Number(Fraction::new(1u64, 10u64))
        );
    }
    // ------------------------------------------------------------------------
    #[test]
    fn symbol_fail() {
        match Lexer::new().symbol(&mut Cursor::<_>::new(b"#t")) {
            Err(Error::LexerFail) => {}
            Err(x) => {
                panic!("{x}")
            }
            _ => {
                panic!()
            }
        }
    }
    // ========================================================================
    #[test]
    fn string_literal_0() {
        assert_eq!(
            Lexer::new()
                .string_literal(&mut Cursor::<_>::new(b"\"\""))
                .unwrap(),
            Token::Str(String::new())
        );
    }
    // ------------------------------------------------------------------------
    #[test]
    fn string_literal_1() {
        assert_eq!(
            Lexer::new()
                .string_literal(&mut Cursor::<_>::new(br#""\"\n\r\t\0""#))
                .unwrap(),
            Token::Str("\"\n\r\t\0".to_string())
        );
    }
    // ------------------------------------------------------------------------
    #[test]
    fn string_literal_fail() {
        match Lexer::new().symbol(&mut Cursor::<_>::new(b"#t")) {
            Err(Error::LexerFail) => {}
            Err(x) => {
                panic!("{x}")
            }
            _ => {
                panic!()
            }
        }
    }
}
