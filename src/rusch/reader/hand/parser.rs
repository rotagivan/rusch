// -*- mode:rust;coding: utf-8-unix; -*-

//! parser.rs

//  Copyright 2017 hanepjiv
//  @author hanepjiv <hanepjiv@gmail.com>
//  @copyright The MIT License (MIT) / Apache License Version 2.0
//  @since 2018/07/08
//  @date 2025/03/08

// ////////////////////////////////////////////////////////////////////////////
// use  =======================================================================
use std::io::Read;
// ----------------------------------------------------------------------------
use super::{
    Atom, Error, Lexer, Op, Reg, Reserved, Result, RuSchError, SECD, Token,
};
// ////////////////////////////////////////////////////////////////////////////
// ============================================================================
/// enum Val
#[allow(variant_size_differences)]
#[derive(Debug, Clone, PartialEq, PartialOrd, Eq, Hash)]
enum Val {
    /// None
    None,
    /// EOF
    #[allow(clippy::upper_case_acronyms)]
    EOF,
    /// Datum
    Datum(usize),
    /// List
    List(usize),
}
// ////////////////////////////////////////////////////////////////////////////
// ============================================================================
/// enum Ret
#[allow(variant_size_differences)]
#[derive(Debug, Clone, PartialEq, PartialOrd, Eq, Hash)]
enum Ret {
    /// Accept
    Accept(Val),
    /// Reject
    Reject(u32, (usize, usize)),
}
// ////////////////////////////////////////////////////////////////////////////
// ============================================================================
/// struct Parser
pub(crate) struct Parser {
    /// lexer
    lexer: Lexer,
    /// cache
    cache: Vec<Token>,
}
// ============================================================================
impl Parser {
    // ========================================================================
    /// fn new
    pub(crate) fn new() -> Self {
        #[cfg(debug_assertions)]
        log::debug!("{}: new", line!());
        Self {
            lexer: Lexer::new(),
            cache: Vec::default(),
        }
    }
    // ========================================================================
    /// clear
    pub(crate) fn clear(&mut self) -> &mut Self {
        #[cfg(debug_assertions)]
        log::debug!("{}: clear", line!());
        self.lexer.clear();
        self.cache.clear();
        self.cache.shrink_to_fit();
        self
    }
    // ========================================================================
    /// fn push
    fn push(&mut self, x: Token) {
        #[cfg(debug_assertions)]
        log::debug!("{}: push", line!());
        self.cache.push(x);
    }
    // ------------------------------------------------------------------------
    /// fn pop
    fn pop(&mut self, read: &mut impl Read) -> Result<Token> {
        #[cfg(debug_assertions)]
        log::debug!("{}: pop", line!());
        if self.cache.is_empty() {
            self.lexer.token(read)
        } else {
            Ok(self.cache.pop().unwrap())
        }
    }
    // ========================================================================
    /// fn err
    fn err(&self, _: RuSchError) -> Error {
        let (line, column) = self.lexer.line_column();
        Error::RuSch { line, column }
    }
    // ========================================================================
    /// fn parse
    pub(crate) fn parse(
        &mut self,
        secd: &mut SECD,
        read: &mut impl Read,
    ) -> Result<&mut Self> {
        #[cfg(debug_assertions)]
        log::debug!("{}: parse", line!());
        secd.top_level();
        match self.input(secd, read)? {
            Ret::Reject(id, (line, column)) => {
                Err(Error::ParserFail { id, line, column })
            }
            Ret::Accept(_) => Ok(self.clear()),
        }
    }
    // ========================================================================
    /// fn input
    fn input(&mut self, secd: &mut SECD, read: &mut impl Read) -> Result<Ret> {
        #[cfg(debug_assertions)]
        log::debug!("{}: input", line!());
        self.expr_tail(secd, read)
    }
    // ========================================================================
    /// fn `expr_tail`
    fn expr_tail(
        &mut self,
        secd: &mut SECD,
        read: &mut impl Read,
    ) -> Result<Ret> {
        #[cfg(debug_assertions)]
        log::debug!("{}: expr_tail", line!());
        match self.expr(secd, read)? {
            x @ (Ret::Reject(_, _) | Ret::Accept(Val::EOF)) => Ok(x),
            Ret::Accept(_) => self.expr_tail(secd, read),
        }
    }
    // ------------------------------------------------------------------------
    /// fn expr
    fn expr(&mut self, secd: &mut SECD, read: &mut impl Read) -> Result<Ret> {
        #[cfg(debug_assertions)]
        log::debug!("{}: expr", line!());
        let t = self.pop(read)?;
        if t == Token::EOF {
            Ok(Ret::Accept(Val::EOF))
        } else {
            self.push(t);
            match self.datum(secd, read)? {
                x @ Ret::Reject(_, _) => Ok(x),
                Ret::Accept(Val::Datum(x)) => {
                    if 0usize != x {
                        let _ = secd
                            .push(Reg::Code, Op::Pop(Reg::Stack))
                            .map_err(|e| self.err(e))?
                            .push(Reg::Code, Op::DumpLoad)
                            .map_err(|e| self.err(e))?
                            .push(Reg::Code, Op::Eval)
                            .map_err(|e| self.err(e))?
                            .push(Reg::Code, Op::DumpSave(1usize, 2usize))
                            .map_err(|e| self.err(e))?
                            .run()
                            .map_err(|e| self.err(e))?;
                    }
                    Ok(Ret::Accept(Val::None))
                }
                _ => Err(Error::ParserInner(line!())),
            }
        }
    }
    // ========================================================================
    /// fn datum
    fn datum(&mut self, secd: &mut SECD, read: &mut impl Read) -> Result<Ret> {
        #[cfg(debug_assertions)]
        log::debug!("{}: datum", line!());
        match self.comment_datum(secd, read)? {
            Ret::Accept(_) => Ok(Ret::Accept(Val::Datum(0usize))),
            Ret::Reject(..) => match self.atom(secd, read)? {
                x @ Ret::Reject(_, _) => Ok(x),
                Ret::Accept(_) => Ok(Ret::Accept(Val::Datum(1usize))),
            },
        }
    }
    // ========================================================================
    /// fn `comment_datum`
    fn comment_datum(
        &mut self,
        secd: &mut SECD,
        read: &mut impl Read,
    ) -> Result<Ret> {
        #[cfg(debug_assertions)]
        log::debug!("{}: comment_datum", line!());
        let t = self.pop(read)?;
        if t == Token::CommentDatum {
            match self.atom(secd, read)? {
                x @ Ret::Reject(_, _) => Ok(x),
                Ret::Accept(_) => {
                    let _ = secd.pop(Reg::Stack).map_err(|e| self.err(e))?;
                    Ok(Ret::Accept(Val::None))
                }
            }
        } else {
            self.push(t);
            Ok(Ret::Reject(line!(), self.lexer.line_column()))
        }
    }
    // ========================================================================
    /// fn atom
    #[allow(clippy::too_many_lines)]
    fn atom(&mut self, secd: &mut SECD, read: &mut impl Read) -> Result<Ret> {
        #[cfg(debug_assertions)]
        log::debug!("{}: atom", line!());
        let t = self.pop(read)?;
        match t {
            Token::Nil => {
                let _ = secd
                    .push(Reg::Stack, Atom::Nil)
                    .map_err(|e| self.err(e))?;
                Ok(Ret::Accept(Val::None))
            }
            Token::Bool(x) => {
                let _ = secd.push(Reg::Stack, x).map_err(|e| self.err(e))?;
                Ok(Ret::Accept(Val::None))
            }
            Token::Number(x) => {
                let _ = secd.push(Reg::Stack, x).map_err(|e| self.err(e))?;
                Ok(Ret::Accept(Val::None))
            }
            Token::Str(x) => {
                let _ = secd
                    .push(Reg::Stack, Atom::Str(x))
                    .map_err(|e| self.err(e))?;
                Ok(Ret::Accept(Val::None))
            }
            Token::Symbol(x) => {
                let _ = secd
                    .push(Reg::Stack, Atom::Symbol(x))
                    .map_err(|e| self.err(e))?;
                Ok(Ret::Accept(Val::None))
            }
            Token::Reserved(x) => {
                let _ = secd.push(Reg::Stack, x).map_err(|e| self.err(e))?;
                Ok(Ret::Accept(Val::None))
            }
            Token::ParenL => match self.list(secd, read)? {
                x @ Ret::Reject(_, _) => Ok(x),
                Ret::Accept(Val::List(x)) => {
                    for _ in 0..x {
                        let _ =
                            secd.xcons(Reg::Stack).map_err(|e| self.err(e))?;
                    }
                    Ok(Ret::Accept(Val::None))
                }
                _ => Err(Error::ParserInner(line!())),
            },
            Token::ParenR => {
                let (line, column) = self.lexer.line_column();
                Err(Error::ParserParenRight { line, column })
            }
            Token::QuoteMark => match self.atom(secd, read)? {
                x @ Ret::Reject(_, _) => Ok(x),
                Ret::Accept(_) => {
                    let _ = secd
                        .push(Reg::Stack, Atom::Nil)
                        .map_err(|e| self.err(e))?
                        .xcons(Reg::Stack)
                        .map_err(|e| self.err(e))?
                        .push(Reg::Stack, Reserved::Quote)
                        .map_err(|e| self.err(e))?
                        .cons(Reg::Stack)
                        .map_err(|e| self.err(e))?;
                    Ok(Ret::Accept(Val::None))
                }
            },
            Token::QuasiquoteMark => match self.atom(secd, read)? {
                x @ Ret::Reject(_, _) => Ok(x),
                Ret::Accept(_) => {
                    let _ = secd
                        .push(Reg::Stack, Atom::Nil)
                        .map_err(|e| self.err(e))?
                        .xcons(Reg::Stack)
                        .map_err(|e| self.err(e))?
                        .push(Reg::Stack, Reserved::Quasiquote)
                        .map_err(|e| self.err(e))?
                        .cons(Reg::Stack)
                        .map_err(|e| self.err(e))?;
                    Ok(Ret::Accept(Val::None))
                }
            },
            Token::UnquoteMark => match self.atom(secd, read)? {
                x @ Ret::Reject(_, _) => Ok(x),
                Ret::Accept(_) => {
                    let _ = secd
                        .push(Reg::Stack, Atom::Nil)
                        .map_err(|e| self.err(e))?
                        .xcons(Reg::Stack)
                        .map_err(|e| self.err(e))?
                        .push(Reg::Stack, Reserved::Unquote)
                        .map_err(|e| self.err(e))?
                        .cons(Reg::Stack)
                        .map_err(|e| self.err(e))?;
                    Ok(Ret::Accept(Val::None))
                }
            },
            Token::UnqSplMark => match self.atom(secd, read)? {
                x @ Ret::Reject(_, _) => Ok(x),
                Ret::Accept(_) => {
                    let _ = secd
                        .push(Reg::Stack, Atom::Nil)
                        .map_err(|e| self.err(e))?
                        .xcons(Reg::Stack)
                        .map_err(|e| self.err(e))?
                        .push(Reg::Stack, Reserved::UnqSpl)
                        .map_err(|e| self.err(e))?
                        .cons(Reg::Stack)
                        .map_err(|e| self.err(e))?;
                    Ok(Ret::Accept(Val::None))
                }
            },
            _ => {
                log::error!("{}", t);
                self.push(t);
                Ok(Ret::Reject(line!(), self.lexer.line_column()))
            }
        }
    }
    // ========================================================================
    /// fn list
    fn list(&mut self, secd: &mut SECD, read: &mut impl Read) -> Result<Ret> {
        #[cfg(debug_assertions)]
        log::debug!("{}: list", line!());
        let t = self.pop(read)?;
        if t == Token::ParenR {
            let _ =
                secd.push(Reg::Stack, Atom::Nil).map_err(|e| self.err(e))?;
            Ok(Ret::Accept(Val::List(0usize)))
        } else {
            self.push(t);
            loop {
                match self.datum(secd, read)? {
                    x @ Ret::Reject(_, _) => {
                        return Ok(x);
                    }
                    Ret::Accept(Val::Datum(0usize)) => {}
                    Ret::Accept(Val::Datum(1usize)) => {
                        match self.list_tail(secd, read)? {
                            x @ Ret::Reject(_, _) => {
                                return Ok(x);
                            }
                            Ret::Accept(Val::List(y)) => {
                                return Ok(Ret::Accept(Val::List(1usize + y)));
                            }
                            _ => {
                                return Err(Error::ParserInner(line!()));
                            }
                        }
                    }
                    _ => {
                        return Err(Error::ParserInner(line!()));
                    }
                }
            }
        }
    }
    // ========================================================================
    /// fn `list_tail`
    fn list_tail(
        &mut self,
        secd: &mut SECD,
        read: &mut impl Read,
    ) -> Result<Ret> {
        #[cfg(debug_assertions)]
        log::debug!("{}: list_tail", line!());
        let t = self.pop(read)?;
        match t {
            Token::ParenR => {
                let _ = secd
                    .push(Reg::Stack, Atom::Nil)
                    .map_err(|e| self.err(e))?;
                Ok(Ret::Accept(Val::List(0usize)))
            }
            Token::Reserved(Reserved::Dot) => loop {
                match self.datum(secd, read)? {
                    x @ Ret::Reject(_, _) => {
                        self.push(t);
                        return Ok(x);
                    }
                    Ret::Accept(Val::Datum(0usize)) => {}
                    Ret::Accept(Val::Datum(1usize)) => {
                        let pr = self.pop(read)?;
                        if pr != Token::ParenR {
                            let (line, column) = self.lexer.line_column();
                            return Err(Error::ParserInvalidImproper {
                                line,
                                column,
                            });
                        }
                        return Ok(Ret::Accept(Val::List(0usize)));
                    }
                    _ => {
                        return Err(Error::ParserInner(line!()));
                    }
                }
            },
            _ => {
                self.push(t);
                match self.datum(secd, read)? {
                    x @ Ret::Reject(_, _) => Ok(x),
                    Ret::Accept(Val::Datum(x)) => {
                        match self.list_tail(secd, read)? {
                            x @ Ret::Reject(_, _) => Ok(x),
                            Ret::Accept(Val::List(y)) => {
                                Ok(Ret::Accept(Val::List(x + y)))
                            }
                            _ => Err(Error::ParserInner(line!())),
                        }
                    }
                    _ => Err(Error::ParserInner(line!())),
                }
            }
        }
    }
}
