// -*- mode:rust;coding:utf-8-unix; -*-

//! error.rs

//  Copyright 2016 hanepjiv
//  @author hanepjiv <hanepjiv@gmail.com>
//  @copyright The MIT License (MIT) / Apache License Version 2.0
//  @since 2016/12/03
//  @date 2025/03/08

// ////////////////////////////////////////////////////////////////////////////
// use  =======================================================================
use std::path::PathBuf;
// ----------------------------------------------------------------------------
use super::ReaderError;
// ////////////////////////////////////////////////////////////////////////////
// ============================================================================
/// enum Error
#[derive(Debug)]
pub enum Error {
    /// IO
    IO(std::io::Error),
    /// `GetOpts`
    GetOpts(getopts::Fail),
    /// `WeakAtomUpgrade`
    WeakAtomUpgrade(String, u32),
    /// `NotCons`
    NotCons(String, u32),
    /// `NotCallable`
    NotCallable(String),
    /// `NotApplicable`
    NotApplicable(String),
    /// `InvalidArenaLimit`
    InvalidArenaLimit,
    /// `InvalidCode`
    InvalidCode(String),
    /// `InvalidDump`
    InvalidDump,
    /// `InvalidQqlv`
    InvalidQqlv,
    /// `InvalidFormals`
    InvalidFormals(u32, String),
    /// `InvalidArg`
    InvalidArg(u32),
    /// Unbound
    Unbound(String),
    /// Reader
    Reader,
    /// `IOFsFileOpen`
    IOFsFileOpen(PathBuf),
}
// ============================================================================
impl From<std::io::Error> for Error {
    fn from(e: std::io::Error) -> Self {
        Self::IO(e)
    }
}
// ----------------------------------------------------------------------------
impl From<getopts::Fail> for Error {
    fn from(e: getopts::Fail) -> Self {
        Self::GetOpts(e)
    }
}
// ----------------------------------------------------------------------------
impl From<ReaderError> for Error {
    fn from(_: ReaderError) -> Self {
        Self::Reader
    }
}
// ============================================================================
impl std::fmt::Display for Error {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{self:?}")
    }
}
// ============================================================================
impl std::error::Error for Error {
    // ========================================================================
    fn source(&self) -> Option<&(dyn std::error::Error + 'static)> {
        match *self {
            Self::IO(ref e) => Some(e),
            Self::GetOpts(ref e) => Some(e),
            Self::WeakAtomUpgrade(..)
            | Self::NotCons(..)
            | Self::NotCallable(..)
            | Self::NotApplicable(..)
            | Self::InvalidArenaLimit
            | Self::InvalidCode(..)
            | Self::InvalidDump
            | Self::InvalidQqlv
            | Self::InvalidFormals(..)
            | Self::InvalidArg(..)
            | Self::Unbound(..)
            | Self::Reader
            | Self::IOFsFileOpen(..) => None,
        }
    }
}
// ////////////////////////////////////////////////////////////////////////////
// ============================================================================
/// type Result
pub type Result<T> = std::result::Result<T, Error>;
// ////////////////////////////////////////////////////////////////////////////
// ============================================================================
#[cfg(test)]
mod tests {
    // use  ===================================================================
    use super::*;
    // ========================================================================
    #[test]
    const fn test_send() {
        const fn assert_send<T: Send>() {}
        assert_send::<Error>();
        assert_send::<Result<()>>();
    }
    // ------------------------------------------------------------------------
    #[test]
    const fn test_sync() {
        const fn assert_sync<T: Sync>() {}
        assert_sync::<Error>();
        assert_sync::<Result<()>>();
    }
}
