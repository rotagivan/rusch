// -*- mode:rust; coding:utf-8-unix; -*-

//! secd.rs

//  Copyright 2017 hanepjiv
//  @author hanepjiv <hanepjiv@gmail.com>
//  @copyright The MIT License (MIT) / Apache License Version 2.0
//  @since 2018/07/05
//  @date 2025/03/09

// ////////////////////////////////////////////////////////////////////////////
// use  =======================================================================
use std::collections::{BTreeMap, VecDeque};
// ----------------------------------------------------------------------------
use fraction::Fraction;
// ----------------------------------------------------------------------------
use super::{
    Atom, AtomClass, Error, Op, OpMixed, RcAtom, Reg, Reserved, Result,
    Stacked, WeakAtom,
};
// ////////////////////////////////////////////////////////////////////////////
// ============================================================================
/// const PHI = `5f64.sqrt()` * 0.5f64 + 0.5f64;
const PHI: f64 = 1.618_033_988_749_895;
// const PHI2 = PHI.powf(2f64);
// const PHI2: f64 = 2.618_033_988_749_895;
/// const `PHI_INV` = 1f64 / PI;
const PHI_INV: f64 = 0.618_033_988_749_894_8;
/// const `PHI_INV2` = `PHI_INV.powf(2f64)`;
const PHI_INV2: f64 = 0.381_966_011_250_105_1;
// ----------------------------------------------------------------------------
/// const `ARENA_LIMIT_MIN`
pub(crate) const ARENA_LIMIT_MIN: usize = 1024usize;
// ============================================================================
/// const `OPERATORS_SYSTEM`
const OPERATORS_SYSTEM: &[(Op, &str)] = &[
    (Op::Print, "print"),
    (Op::Display, "display"),
    (Op::DisplayLn, "displayln"),
    (Op::NewLine, "newline"),
    (Op::Eval, "eval"),
    (Op::Cons(Reg::Stack), "cons"),
    (Op::Car(Reg::Stack), "car"),
    (Op::Cdr(Reg::Stack), "cdr"),
    (Op::List(Reg::Stack), "list"),
    (Op::EQ, "=="),
    (Op::NE, "!="),
    (Op::LT, "<"),
    (Op::GT, ">"),
    (Op::LE, "<="),
    (Op::GE, ">="),
    (Op::Mul, "*"),
    (Op::Div, "/"),
    (Op::Add, "+"),
    (Op::Sub, "-"),
    (Op::Mixed(OpMixed::Begin), "begin"),
];
// ////////////////////////////////////////////////////////////////////////////
// ============================================================================
#[doc(hidden)]
#[allow(clippy::upper_case_acronyms)]
#[derive(Debug, Clone)]
pub(crate) struct SECD {
    /// arena
    arena: VecDeque<RcAtom>,
    /// We need to use `RcAtom` instead of `WeakAtom`,
    /// for `weak_count()` in `gc_arena()`.
    cache_symbols: BTreeMap<String, RcAtom>,
    /// We need to use `RcAtom` instead of `WeakAtom`,
    /// for `weak_count()` in `gc_arena()`.
    cache_reserveds: BTreeMap<Reserved, RcAtom>,
    /// stack
    stack: WeakAtom,
    /// env
    env: WeakAtom,
    /// code
    code: WeakAtom,
    /// dump
    dump: WeakAtom,
    /// work
    work: WeakAtom,
    /// nil
    nil: WeakAtom,
    /// undef
    undef: WeakAtom,
    /// t
    t: WeakAtom,
    /// f
    f: WeakAtom,
    /// `arena_limit`
    arena_limit: usize,
    /// qqlv
    qqlv: usize,
}
// ============================================================================
impl std::fmt::Display for SECD {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        writeln!(
            f,
            r"
  SECD {{
    arena       : {0} / {1}
    qqlv        : {7}
    stack       : {2}
    env         : {3}
    code        : {4}
    dump        : {5}
    work        : {6}
  }}",
            self.arena.len(),
            self.arena_limit,
            self.stack,
            self.env,
            self.code,
            self.dump,
            self.work,
            self.qqlv,
        )
    }
}
// ----------------------------------------------------------------------------
impl SECD {
    // ========================================================================
    /// fn `arena_info`
    #[allow(dead_code)]
    pub(crate) fn arena_info(&self) -> (usize, usize) {
        (self.arena.len(), self.arena_limit)
    }
    // ========================================================================
    /// fn new
    pub(crate) fn new(arena_limit: usize) -> Result<Self> {
        #[cfg(debug_assertions)]
        log::debug!("{}: new", line!());

        if arena_limit < ARENA_LIMIT_MIN {
            return Err(Error::InvalidArenaLimit);
        }

        let mut arena = VecDeque::default();

        let (nil, undef, t, f) = {
            let mut push_arena = |atom| {
                let rc = RcAtom::new(atom);
                arena.push_back(rc.clone());
                rc.downgrade()
            };
            (
                push_arena(Atom::Nil),
                push_arena(Atom::Undef),
                push_arena(Atom::Bool(true)),
                push_arena(Atom::Bool(false)),
            )
        };

        let mut secd = Self {
            arena,
            cache_symbols: BTreeMap::default(),
            cache_reserveds: BTreeMap::default(),
            stack: nil.clone(),
            env: nil.clone(),
            code: nil.clone(),
            dump: nil.clone(),
            work: nil.clone(),
            nil,
            undef,
            t,
            f,
            arena_limit,
            qqlv: 0usize,
        };

        let _ = secd.clear()?;

        Ok(secd)
    }
    // ========================================================================
    /// fn `top_level`
    pub(crate) fn top_level(&mut self) {
        #[cfg(debug_assertions)]
        log::debug!("{}: top_level", line!());
        self.stack = self.nil.clone();
        self.code = self.nil.clone();
        self.work = self.nil.clone();
        self.qqlv = 0usize;
    }
    // ========================================================================
    /// fn clear
    pub(crate) fn clear(&mut self) -> Result<&mut Self> {
        #[cfg(debug_assertions)]
        log::debug!("{}: clear", line!());
        self.top_level();
        self.env = self.nil.clone();
        self.dump = self.nil.clone();

        // env system
        let _ = self.push(Reg::Env, Atom::Nil)?;
        for (op, sym) in OPERATORS_SYSTEM {
            let _ = self
                .push(Reg::Stack, op.clone())?
                .push(Reg::Stack, Atom::Symbol((*sym).to_string()))?
                .define_impl()?;
        }

        // env user
        let _ = self.push(Reg::Env, Atom::Nil)?;

        let _ = self.gc_to_fit();

        Ok(self)
    }
    // ========================================================================
    #[allow(
        clippy::cast_precision_loss,
        clippy::cast_sign_loss,
        clippy::cast_possible_truncation
    )]
    fn update_arena_limit(&mut self, len: usize) {
        #[cfg(debug_assertions)]
        log::debug!("{}: update_arena_limit", line!());
        let old = self.arena_limit;
        self.arena_limit = ((len as f64) * PHI) as usize;
        if self.arena_limit < ARENA_LIMIT_MIN {
            self.arena_limit = ARENA_LIMIT_MIN;
        } else if old != self.arena_limit {
            log::info!("arena limit {} -> {}", old, self.arena_limit);
        }
    }
    // ------------------------------------------------------------------------
    #[allow(
        clippy::cast_precision_loss,
        clippy::cast_sign_loss,
        clippy::cast_possible_truncation
    )]
    fn gc_arena(&mut self) -> (usize, usize) {
        let start = self.arena.len();

        // self.arena.drain_filter(|x| x.weak_count() == 0);
        self.arena = self
            .arena
            .iter()
            .filter(|x| x.weak_count() != 0)
            .cloned()
            .collect();

        let len = self.arena.len();

        if start == len {
            self.update_arena_limit(len);
        } else {
            log::info!("arena length {} -> {}", start, len);
            if (start - len)
                < (((start as f64) * PHI_INV2 * PHI_INV2) as usize)
            {
                self.update_arena_limit(len);
            }
            if len < (((self.arena_limit as f64) * PHI_INV) as usize) {
                self.update_arena_limit(len);
            }
        }

        (start, len)
    }
    // ------------------------------------------------------------------------
    /// fn gc
    pub(crate) fn gc(&mut self) -> &mut Self {
        #[cfg(debug_assertions)]
        log::debug!("{}: gc", line!());
        let (_, _) = self.gc_arena();
        self
    }
    // ------------------------------------------------------------------------
    /// fn `gc_to_fit`
    pub(crate) fn gc_to_fit(&mut self) -> &mut Self {
        #[cfg(debug_assertions)]
        log::debug!("{}: gc_top_fit", line!());
        let (start, mut len) = self.gc_arena();
        if start != len {
            loop {
                let (_, l) = self.gc_arena();
                if len == l {
                    break;
                }
                len = l;
            }
        }
        self
    }
    // ========================================================================
    fn arena_push(&mut self, atom: RcAtom) -> WeakAtom {
        #[cfg(debug_assertions)]
        log::debug!("{}: arena_push", line!());
        let weak = atom.downgrade();
        self.arena.push_back(atom);
        if self.arena_limit < self.arena.len() {
            let _ = self.gc();
        }
        weak
    }
    // ========================================================================
    /// fn `new_nil`
    pub(crate) fn new_nil(&self) -> WeakAtom {
        #[cfg(debug_assertions)]
        log::debug!("{}: new_nil", line!());
        self.nil.clone()
    }
    // ------------------------------------------------------------------------
    /// fn `new_undef`
    pub(crate) fn new_undef(&self) -> WeakAtom {
        #[cfg(debug_assertions)]
        log::debug!("{}: new_undef", line!());
        self.undef.clone()
    }
    // ------------------------------------------------------------------------
    /// fn `new_op`
    pub(crate) fn new_op(&mut self, x: Op) -> WeakAtom {
        #[cfg(debug_assertions)]
        log::debug!("{}: new_op", line!());
        self.arena_push(RcAtom::new(x.into()))
    }
    // ------------------------------------------------------------------------
    /// fn `new_bool`
    pub(crate) fn new_bool(&self, x: bool) -> WeakAtom {
        #[cfg(debug_assertions)]
        log::debug!("{}: new_bool", line!());
        if x { self.t.clone() } else { self.f.clone() }
    }
    // ------------------------------------------------------------------------
    /// fn `new_num`
    pub(crate) fn new_num(&mut self, x: Fraction) -> WeakAtom {
        #[cfg(debug_assertions)]
        log::debug!("{}: new_num", line!());
        self.arena_push(RcAtom::new(x.into()))
    }
    // ------------------------------------------------------------------------
    /// fn `new_str`
    pub(crate) fn new_str(&mut self, x: String) -> WeakAtom {
        #[cfg(debug_assertions)]
        log::debug!("{}: new_str", line!());
        self.arena_push(RcAtom::new(Atom::Str(x)))
    }
    // ------------------------------------------------------------------------
    /// fn `new_symbol`
    pub(crate) fn new_symbol(&mut self, x: String) -> WeakAtom {
        #[cfg(debug_assertions)]
        log::debug!("{}: new_symbol", line!());
        if let Some(rc) = self.cache_symbols.get(&x) {
            return rc.downgrade();
        }
        let rc = RcAtom::new(Atom::Symbol(x.clone()));
        drop(self.cache_symbols.insert(x, rc.clone()));
        rc.downgrade()
    }
    // ------------------------------------------------------------------------
    /// fn `new_reserved`
    pub(crate) fn new_reserved(&mut self, x: Reserved) -> WeakAtom {
        #[cfg(debug_assertions)]
        log::debug!("{}: new_reserved", line!());
        if let Some(rc) = self.cache_reserveds.get(&x) {
            return rc.downgrade();
        }
        let rc = RcAtom::new(Atom::Reserved(x));
        drop(self.cache_reserveds.insert(x, rc.clone()));
        rc.downgrade()
    }
    // ------------------------------------------------------------------------
    /// fn `new_cons`
    #[allow(clippy::similar_names)]
    pub(crate) fn new_cons(
        &mut self,
        car: WeakAtom,
        cdr: WeakAtom,
    ) -> WeakAtom {
        #[cfg(debug_assertions)]
        log::debug!("{}: new_cons", line!());
        self.arena_push(RcAtom::new(Atom::Cons { car, cdr }))
    }
    // ------------------------------------------------------------------------
    /// fn `new_continue`
    pub(crate) fn new_continue(
        &mut self,
        stack: WeakAtom,
        env: WeakAtom,
        code: WeakAtom,
        dump: WeakAtom,
        work: WeakAtom,
        qqlv: usize,
    ) -> WeakAtom {
        #[cfg(debug_assertions)]
        log::debug!("{}: new_continue", line!());
        self.arena_push(RcAtom::new(Atom::Continue {
            stack,
            env,
            code,
            dump,
            work,
            qqlv,
        }))
    }
    // ------------------------------------------------------------------------
    /// fn `new_procedure`
    pub(crate) fn new_procedure(
        &mut self,
        env: WeakAtom,
        formals: WeakAtom,
        body: WeakAtom,
    ) -> WeakAtom {
        #[cfg(debug_assertions)]
        log::debug!("{}: new_procedure", line!());
        self.arena_push(RcAtom::new(Atom::Procedure { env, formals, body }))
    }
    // ========================================================================
    /// fn `reg_get`
    const fn reg_get(&self, reg: Reg) -> &WeakAtom {
        match reg {
            Reg::Stack => &self.stack,
            Reg::Env => &self.env,
            Reg::Code => &self.code,
            Reg::Dump => &self.dump,
            Reg::Work => &self.work,
            Reg::Nil => &self.nil,
        }
    }
    // ------------------------------------------------------------------------
    /// fn `reg_set`
    fn reg_set(&mut self, reg: Reg, x: WeakAtom) {
        #[cfg(debug_assertions)]
        log::debug!("{}: reg_set", line!());
        match reg {
            Reg::Stack => self.stack = x,
            Reg::Env => self.env = x,
            Reg::Code => self.code = x,
            Reg::Dump => self.dump = x,
            Reg::Work => self.work = x,
            Reg::Nil => self.nil = x,
        }
    }
    // ========================================================================
    /// fn top
    fn top(&self, reg: Reg) -> Result<WeakAtom> {
        #[cfg(debug_assertions)]
        log::debug!("{}: top", line!());
        let weak = self.reg_get(reg);
        let rc = weak.upgrade()?;
        let atom = rc.as_ref().borrow();
        match *atom {
            Atom::Nil => Ok(weak.clone()),
            Atom::Cons { ref car, .. } => Ok(car.clone()),
            _ => Err(Error::NotCons(file!().to_string(), line!())),
        }
    }
    // ========================================================================
    /// fn push
    #[allow(clippy::similar_names)]
    pub(crate) fn push(
        &mut self,
        reg: Reg,
        x: impl Stacked,
    ) -> Result<&mut Self> {
        #[cfg(debug_assertions)]
        log::debug!("{}: push", line!());
        let car = x.to_weak(self)?;
        let cdr = self.reg_get(reg).clone();
        let cons = self.new_cons(car, cdr);
        self.reg_set(reg, cons);
        Ok(self)
    }
    // ========================================================================
    /// fn pop
    pub(crate) fn pop(&mut self, reg: Reg) -> Result<&mut Self> {
        #[cfg(debug_assertions)]
        log::debug!("{}: pop", line!());
        let rc = self.reg_get(reg).upgrade()?;
        let atom = rc.as_ref().borrow();
        match *atom {
            Atom::Nil => Ok(self),
            Atom::Cons { ref cdr, .. } => {
                self.reg_set(reg, cdr.clone());
                Ok(self)
            }
            _ => Err(Error::NotCons(file!().to_string(), line!())),
        }
    }
    // ========================================================================
    /// fn copy
    #[allow(dead_code)]
    pub(crate) fn copy(&mut self, reg: Reg) -> Result<&mut Self> {
        #[cfg(debug_assertions)]
        log::debug!("{}: copy", line!());
        let rc = self.reg_get(reg).upgrade()?;
        let atom = rc.as_ref().borrow_mut();
        match *atom {
            Atom::Cons { ref car, .. } => self.push(reg, car.clone()),
            _ => Err(Error::NotCons(file!().to_string(), line!())),
        }
    }
    // ========================================================================
    /// fn press
    pub(crate) fn press(&mut self, reg: Reg) -> Result<&mut Self> {
        #[cfg(debug_assertions)]
        log::debug!("{}: press", line!());
        let rc = self.reg_get(reg).upgrade()?;
        let mut atom = rc.as_ref().borrow_mut();
        match &mut *atom {
            &mut Atom::Nil => Ok(self),
            ref mut atom @ Atom::Cons { .. } => {
                let cdr_rc = atom.cdr()?.upgrade()?;
                let cdr_atom = cdr_rc.as_ref().borrow();
                let _ = atom.set_cdr(cdr_atom.cdr()?)?;
                Ok(self)
            }
            _ => Err(Error::NotCons(file!().to_string(), line!())),
        }
    }
    // ========================================================================
    /// fn swap
    pub(crate) fn swap(&mut self, reg: Reg) -> Result<&mut Self> {
        #[cfg(debug_assertions)]
        log::debug!("{}: swap", line!());
        let rc = self.reg_get(reg).upgrade()?;
        let atom = rc.as_ref().borrow_mut();
        match *atom {
            Atom::Cons { ref cdr, .. } => {
                self.push(reg, cdr.clone())?.press(reg)
            }
            _ => Err(Error::NotCons(file!().to_string(), line!())),
        }
    }
    // ========================================================================
    /// fn head
    pub(crate) fn head(&mut self, reg: Reg) -> Result<&mut Self> {
        #[cfg(debug_assertions)]
        log::debug!("{}: head", line!());
        let top_rc = self.top(reg)?.upgrade()?;
        let top_atom = top_rc.as_ref().borrow();
        match *top_atom {
            Atom::Cons { ref car, ref cdr } => self
                .pop(reg)?
                .push(reg, cdr.clone())?
                .push(reg, car.clone()),
            _ => Err(Error::NotCons(file!().to_string(), line!())),
        }
    }
    // ========================================================================
    /// fn cons
    pub(crate) fn cons(&mut self, reg: Reg) -> Result<&mut Self> {
        #[cfg(debug_assertions)]
        log::debug!("{}: cons", line!());
        let rc = self.reg_get(reg).upgrade()?;
        let atom = rc.as_ref().borrow();
        self.pop(reg)?
            .pop(reg)?
            .push(reg, Atom::new_cons(atom.car()?, atom.cadr()?))
    }
    // ------------------------------------------------------------------------
    /// fn xcons
    pub(crate) fn xcons(&mut self, reg: Reg) -> Result<&mut Self> {
        #[cfg(debug_assertions)]
        log::debug!("{}: xcons", line!());
        let rc = self.reg_get(reg).upgrade()?;
        let atom = rc.as_ref().borrow();
        self.pop(reg)?
            .pop(reg)?
            .push(reg, Atom::new_cons(atom.cadr()?, atom.car()?))
    }
    // ========================================================================
    /// fn car
    pub(crate) fn car(&mut self, reg: Reg) -> Result<&mut Self> {
        #[cfg(debug_assertions)]
        log::debug!("{}: car", line!());
        let rc = self.top(reg)?.upgrade()?;
        let atom = rc.as_ref().borrow();
        self.pop(reg)?.push(reg, atom.car()?)
    }
    // ------------------------------------------------------------------------
    /// fn cdr
    pub(crate) fn cdr(&mut self, reg: Reg) -> Result<&mut Self> {
        #[cfg(debug_assertions)]
        log::debug!("{}: cdr", line!());
        let rc = self.top(reg)?.upgrade()?;
        let atom = rc.as_ref().borrow();
        self.pop(reg)?.push(reg, atom.cdr()?)
    }
    // ========================================================================
    /// fn list
    pub(crate) fn list(&mut self, reg: Reg) -> Result<&mut Self> {
        #[cfg(debug_assertions)]
        log::debug!("{}: list", line!());
        let r = self.reg_get(reg).clone();
        let _ = self.push(reg, r)?;
        let rc = self.reg_get(reg).upgrade()?;
        let mut atom = rc.as_ref().borrow_mut();
        let _ = atom.set_cdr(self.nil.clone())?;
        Ok(self)
    }
    // ------------------------------------------------------------------------
    /// fn unlist
    pub(crate) fn unlist(&mut self, reg: Reg) -> Result<&mut Self> {
        #[cfg(debug_assertions)]
        log::debug!("{}: unlist", line!());
        let weak = self.top(reg)?;
        self.reg_set(reg, weak);
        Ok(self)
    }
    // ========================================================================
    /// fn `move_top`
    pub(crate) fn move_top(
        &mut self,
        from: Reg,
        to: Reg,
    ) -> Result<&mut Self> {
        #[cfg(debug_assertions)]
        log::debug!("{}: move_top", line!());
        let top_weak = self.top(from)?;
        self.pop(from)?.push(to, top_weak)
    }
    // ========================================================================
    /// fn splice
    pub(crate) fn splice(&mut self) -> Result<&mut Self> {
        #[cfg(debug_assertions)]
        log::debug!("{}: splice", line!());
        let mut count = 0usize;
        loop {
            let top_rc = self.top(Reg::Stack)?.upgrade()?;
            let top_atom = top_rc.as_ref().borrow();
            match *top_atom {
                Atom::Nil => break,
                Atom::Cons { ref car, ref cdr } => {
                    // head
                    let rc = self.reg_get(Reg::Stack).upgrade()?;
                    let atom = &mut *rc.as_ref().borrow_mut();
                    let _ = atom.set_car(cdr.clone())?;
                    drop(
                        self.push(Reg::Stack, car.clone())?
                            .move_top(Reg::Stack, Reg::Work),
                    );
                }
                _ => {
                    return Err(Error::NotCons(file!().to_string(), line!()));
                }
            }
            count += 1usize;
        }
        drop(self.pop(Reg::Stack));
        for _ in 0..count {
            drop(self.move_top(Reg::Work, Reg::Stack));
        }
        Ok(self)
    }
    // ========================================================================
    /// fn print
    pub(crate) fn print(&self) -> &Self {
        print!("{self}");
        self
    }
    // ========================================================================
    /// fn display
    pub(crate) fn display(&self) -> Result<&Self> {
        print!("{}", self.top(Reg::Stack)?);
        Ok(self)
    }
    // ------------------------------------------------------------------------
    /// fn displayln
    pub(crate) fn displayln(&self) -> Result<&Self> {
        println!("{}", self.top(Reg::Stack)?);
        Ok(self)
    }
    // ------------------------------------------------------------------------
    /// fn newline
    pub(crate) fn newline(&self) -> &Self {
        println!();
        self
    }
    // ========================================================================
    /// fn `find_variable`
    #[allow(clippy::similar_names)]
    fn find_variable(&mut self) -> Result<()> {
        #[cfg(debug_assertions)]
        log::debug!("{}: find_variable", line!());
        let sym_rc = self.top(Reg::Stack)?.upgrade()?;
        let sym_atom = sym_rc.as_ref().borrow();
        match *sym_atom {
            Atom::Symbol(ref sym_inner) => {
                let mut env_weak = self.reg_get(Reg::Env).clone();
                loop {
                    let env_rc = env_weak.upgrade()?;
                    let env_atom = env_rc.as_ref().borrow();
                    match *env_atom {
                        Atom::Nil => {
                            break;
                        }
                        Atom::Cons {
                            car: ref env_car,
                            cdr: ref env_cdr,
                        } => {
                            let mut cons_weak = env_car.clone();
                            loop {
                                let cons_rc = cons_weak.upgrade()?;
                                let cons_atom = cons_rc.as_ref().borrow();
                                match *cons_atom {
                                    Atom::Nil => {
                                        break;
                                    }
                                    Atom::Cons {
                                        car: ref cons_car,
                                        cdr: ref cons_cdr,
                                    } => {
                                        let var_rc = cons_car.upgrade()?;
                                        let var_atom =
                                            var_rc.as_ref().borrow();
                                        if sym_rc == var_atom.car()? {
                                            let _ =
                                                self.pop(Reg::Stack)?.push(
                                                    Reg::Stack,
                                                    cons_car.clone(),
                                                )?;
                                            return Ok(());
                                        }
                                        cons_weak = cons_cdr.clone();
                                    }
                                    _ => {
                                        return Err(Error::NotCons(
                                            file!().to_string(),
                                            line!(),
                                        ));
                                    }
                                }
                            }
                            env_weak = env_cdr.clone();
                        }
                        _ => {
                            return Err(Error::NotCons(
                                file!().to_string(),
                                line!(),
                            ));
                        }
                    }
                }
                Err(Error::Unbound(sym_inner.clone()))
            }
            _ => Err(Error::InvalidArg(line!())),
        }
    }
    // ------------------------------------------------------------------------
    /// fn find
    pub(crate) fn find(&mut self) -> Result<&mut Self> {
        #[cfg(debug_assertions)]
        log::debug!("{}: find", line!());
        self.find_variable()?;
        let top_rc = self.top(Reg::Stack)?.upgrade()?;
        let top_atom = top_rc.as_ref().borrow();
        self.pop(Reg::Stack)?.push(Reg::Stack, top_atom.cdr()?)
    }
    // ========================================================================
    /// fn `define_impl`
    fn define_impl(&mut self) -> Result<&mut Self> {
        #[cfg(debug_assertions)]
        log::debug!("{}: define_impl", line!());
        {
            let _ = self.cons(Reg::Stack)?;
            let stack_weak = self.reg_get(Reg::Stack).clone();
            let _ = self.pop(Reg::Stack)?;
            let top_rc = self.reg_get(Reg::Env).clone().upgrade()?;
            let mut top_atom = top_rc.as_ref().borrow_mut();
            match *top_atom {
                Atom::Cons { ref car, .. } => {
                    let stack_rc = stack_weak.upgrade()?;
                    let mut stack_atom = stack_rc.as_ref().borrow_mut();
                    let _ = stack_atom.set_cdr(car.clone())?;
                }
                _ => {
                    return Err(Error::NotCons(file!().to_string(), line!()));
                }
            }
            let _ = top_atom.set_car(stack_weak)?;
        }
        Ok(self)
    }
    // ------------------------------------------------------------------------
    /// fn define
    pub(crate) fn define(&mut self) -> Result<SECDMixed<'_>> {
        #[cfg(debug_assertions)]
        log::debug!("{}: define", line!());
        let top_rc = self.top(Reg::Stack)?.upgrade()?;
        let top_atom = top_rc.as_ref().borrow();
        match *top_atom {
            Atom::Symbol(_) => match self.find_variable() {
                Ok(()) => {
                    let t = self.new_bool(true);
                    self.mixed()
                        .push_code(Op::Push(Reg::Stack, t))?
                        .push_code(Op::SetxImpl)?
                        .push_code(Op::DumpLoad)?
                        .push_code(Op::Eval)?
                        .push_code(Op::DumpSave(1usize, 2usize))?
                        .push_code(Op::MoveTop(Reg::Stack, Reg::Work))
                }
                Err(Error::Unbound(_)) => {
                    let t = self.new_bool(true);
                    self.mixed()
                        .push_code(Op::Push(Reg::Stack, t))?
                        .push_code(Op::DefineImpl)?
                        .push_code(Op::MoveTop(Reg::Work, Reg::Stack))?
                        .push_code(Op::DumpLoad)?
                        .push_code(Op::Eval)?
                        .push_code(Op::DumpSave(1usize, 2usize))?
                        .push_code(Op::MoveTop(Reg::Stack, Reg::Work))
                }
                Err(x) => Err(x),
            },
            Atom::Cons { .. } => {
                let t = self.new_bool(true);
                self.mixed()
                    .push_code(Op::Push(Reg::Stack, t))?
                    .push_code(Op::DefineImpl)?
                    .push_code(Op::MoveTop(Reg::Work, Reg::Stack))?
                    .push_code(Op::Reserved(Reserved::Lambda))?
                    .push_code(Op::List(Reg::Stack))?
                    .push_code(Op::MoveTop(Reg::Stack, Reg::Work))?
                    .push_code(Op::Head(Reg::Stack))
            }
            _ => Err(Error::InvalidArg(line!())),
        }
    }
    // ========================================================================
    /// fn `setx_impl`
    pub(crate) fn setx_impl(&mut self) -> Result<&mut Self> {
        #[cfg(debug_assertions)]
        log::debug!("{}: setx", line!());
        {
            let var_rc = self.top(Reg::Work)?.upgrade()?;
            let mut var_atom = var_rc.as_ref().borrow_mut();
            let top_weak = self.top(Reg::Stack)?;
            let _ = var_atom.set_cdr(top_weak)?;
        }
        self.pop(Reg::Work)?.pop(Reg::Stack)
    }
    // ------------------------------------------------------------------------
    /// fn setx
    pub(crate) fn setx(&mut self) -> Result<&mut Self> {
        #[cfg(debug_assertions)]
        log::debug!("{}: setx", line!());
        match self.find_variable() {
            Ok(()) => {
                let t = self.new_bool(true);
                self.push(Reg::Code, Op::Push(Reg::Stack, t))?
                    .push(Reg::Code, Op::SetxImpl)?
                    .push(Reg::Code, Op::Eval)?
                    .push(Reg::Code, Op::MoveTop(Reg::Stack, Reg::Work))
            }
            Err(x) => Err(x),
        }
    }
    // ========================================================================
    /// fn lambda
    fn lambda(&mut self) -> Result<&mut Self> {
        #[cfg(debug_assertions)]
        log::debug!("{}: lambda", line!());
        let env = self.reg_get(Reg::Env).clone();
        let formals = self.head(Reg::Stack)?.top(Reg::Stack)?;
        let _ = self.pop(Reg::Stack)?;
        let body = self.top(Reg::Stack)?;
        let _ = self.pop(Reg::Stack)?;
        self.push(Reg::Stack, Atom::new_procedure(env, formals, body))
    }
    // ========================================================================
    /// fn cond
    fn cond(&mut self) -> Result<&mut Self> {
        #[cfg(debug_assertions)]
        log::debug!("{}: cond", line!());
        let undef = self.new_undef();
        self.push(Reg::Code, Op::Push(Reg::Stack, undef))?
            .push(Reg::Code, Op::Pop(Reg::Stack))?
            // ----------------------------------------------------------------
            .push(Reg::Code, Op::Jump(2usize))?
            .push(Reg::Code, Op::Reserved(Reserved::Cond))?
            .push(Reg::Code, Op::Pop(Reg::Stack))?
            .push(Reg::Code, Op::Pop(Reg::Stack))?
            // ----------------------------------------------------------------
            .push(Reg::Code, Op::Jump(6usize))?
            .push(Reg::Code, Op::Eval)?
            .push(Reg::Code, Op::Unlist(Reg::Stack))?
            .push(Reg::Code, Op::Pop(Reg::Stack))?
            .push(Reg::Code, Op::JumpFalse(4usize))?
            .push(Reg::Code, Op::Eval)?
            .push(Reg::Code, Op::Head(Reg::Stack))?
            .push(Reg::Code, Op::JumpNil(11usize))
    }
    // ========================================================================
    /// fn if_
    fn if_(&mut self) -> Result<&mut Self> {
        #[cfg(debug_assertions)]
        log::debug!("{}: if_", line!());
        let nil = self.new_nil();
        let else_ = self.new_reserved(Reserved::Else);
        self.push(Reg::Code, Op::Reserved(Reserved::Cond))?
            .push(Reg::Code, Op::MoveTop(Reg::Work, Reg::Stack))?
            .push(Reg::Code, Op::Cons(Reg::Stack))?
            .push(Reg::Code, Op::Push(Reg::Stack, else_))?
            .push(Reg::Code, Op::XCons(Reg::Stack))?
            .push(Reg::Code, Op::Push(Reg::Stack, nil.clone()))?
            .push(Reg::Code, Op::JumpNil(4usize))?
            .push(Reg::Code, Op::MoveTop(Reg::Stack, Reg::Work))?
            .push(Reg::Code, Op::Cons(Reg::Stack))?
            .push(Reg::Code, Op::MoveTop(Reg::Work, Reg::Stack))?
            .push(Reg::Code, Op::XCons(Reg::Stack))?
            .push(Reg::Code, Op::Push(Reg::Stack, nil))?
            .push(Reg::Code, Op::MoveTop(Reg::Stack, Reg::Work))
    }
    // ========================================================================
    /// fn `dump_save`
    fn dump_save(
        &mut self,
        offset_stack: usize,
        offset_code: usize,
    ) -> Result<&mut Self> {
        #[cfg(debug_assertions)]
        log::debug!("{}: dump_save", line!());
        let stack = self.stack.at(offset_stack)?;
        let env = self.env.clone();
        let code = self.code.at(offset_code)?;
        let dump = self.dump.clone();
        let work = self.work.clone();
        let qqlv = self.qqlv;
        self.push(
            Reg::Dump,
            Atom::new_continue(stack, env, code, dump, work, qqlv),
        )
    }
    // ------------------------------------------------------------------------
    /// fn `dump_load`
    fn dump_load(&mut self) -> Result<&mut Self> {
        #[cfg(debug_assertions)]
        log::debug!("{}: dump_load", line!());
        let ret = self.top(Reg::Stack)?;
        let dump_rc = self.top(Reg::Dump)?.upgrade()?;
        let dump_atom = dump_rc.as_ref().borrow();
        match *dump_atom {
            Atom::Continue {
                ref stack,
                ref env,
                ref code,
                ref dump,
                ref work,
                ref qqlv,
            } => {
                self.reg_set(Reg::Stack, stack.clone());
                self.reg_set(Reg::Env, env.clone());
                self.reg_set(Reg::Code, code.clone());
                self.reg_set(Reg::Dump, dump.clone());
                self.reg_set(Reg::Work, work.clone());
                self.qqlv = *qqlv;
                self.push(Reg::Stack, ret)
            }
            _ => Err(Error::InvalidDump),
        }
    }
    // ========================================================================
    /// fn `qqlv_inc`
    const fn qqlv_inc(&mut self) -> &mut Self {
        #[cfg(debug_assertions)]
        assert!(self.qqlv < usize::MAX);

        self.qqlv += 1usize;

        self
    }
    // ------------------------------------------------------------------------
    /// fn `qqlv_dec`
    const fn qqlv_dec(&mut self) -> &mut Self {
        #[cfg(debug_assertions)]
        assert!(0usize < self.qqlv);

        self.qqlv -= 1usize;

        self
    }
    // ========================================================================
    /// fn `eval_rest`
    fn eval_rest(&mut self) -> Result<&mut Self> {
        #[cfg(debug_assertions)]
        log::debug!("{}: eval_rest", line!());
        let stack_rc = self.stack.upgrade()?;
        let stack_atom = stack_rc.as_ref().borrow();
        if stack_atom.is(AtomClass::Nil) {
            Ok(self)
        } else {
            let top_rc = stack_atom.car()?.upgrade()?;
            let top_atom = top_rc.as_ref().borrow();
            if top_atom.is(AtomClass::Nil) {
                Ok(self)
            } else if stack_atom.cdr()?.is(AtomClass::Nil)? {
                self.push(Reg::Code, Op::Eval)
            } else {
                self.push(Reg::Code, Op::MoveTop(Reg::Work, Reg::Stack))?
                    .push(Reg::Code, Op::EvalRest)?
                    .push(Reg::Code, Op::MoveTop(Reg::Stack, Reg::Work))?
                    .push(Reg::Code, Op::Eval)
            }
        }
    }
    // ------------------------------------------------------------------------
    /// fn `eval_cons`
    fn eval_cons(&mut self, stack: &Atom) -> Result<&mut Self> {
        #[cfg(debug_assertions)]
        log::debug!("{}: eval_cons", line!());
        if stack.cdr()?.is(AtomClass::Nil)? {
            // tail call
            self.push(Reg::Code, Op::Call)?
                .push(Reg::Code, Op::Eval)?
                .push(Reg::Code, Op::Head(Reg::Stack))
        } else {
            // dump call
            self.push(Reg::Code, Op::DumpLoad)?
                .push(Reg::Code, Op::Call)?
                .push(Reg::Code, Op::Eval)?
                .push(Reg::Code, Op::Head(Reg::Stack))?
                .push(Reg::Code, Op::DumpSave(1usize, 4usize))
        }
    }
    // ------------------------------------------------------------------------
    /// fn `qq_cons`
    fn qq_cons(&mut self, stack: &Atom) -> Result<&mut Self> {
        #[cfg(debug_assertions)]
        log::debug!("{}: qq_cons", line!());
        if stack.cdr()?.is(AtomClass::Nil)? {
            // tail call
            self.push(Reg::Code, Op::List(Reg::Stack))?
                .push(Reg::Code, Op::EvalRest)?
                .push(Reg::Code, Op::Unlist(Reg::Stack))
        } else {
            // dump call
            self.push(Reg::Code, Op::DumpLoad)?
                .push(Reg::Code, Op::List(Reg::Stack))?
                .push(Reg::Code, Op::EvalRest)?
                .push(Reg::Code, Op::Unlist(Reg::Stack))?
                .push(Reg::Code, Op::DumpSave(1usize, 4usize))
        }
    }
    // ------------------------------------------------------------------------
    /// fn eval
    fn eval(&mut self) -> Result<&mut Self> {
        #[cfg(debug_assertions)]
        log::debug!("{}: eval", line!());
        let stack_rc = self.stack.upgrade()?;
        let stack_atom = stack_rc.as_ref().borrow();
        let top_rc = stack_atom.car()?.upgrade()?;
        let top_atom = top_rc.as_ref().borrow();
        match *top_atom {
            Atom::Nil
            | Atom::Undef
            | Atom::Op(_)
            | Atom::Bool(_)
            | Atom::Number(_)
            | Atom::Str(_)
            | Atom::Reserved(_)
            | Atom::Continue { .. }
            | Atom::MultiValue { .. }
            | Atom::Procedure { .. } => Ok(self),
            Atom::Symbol(_) => {
                if 0usize == self.qqlv {
                    self.push(Reg::Code, Op::Find)
                } else {
                    Ok(self)
                }
            }
            Atom::Cons { ref car, .. } => {
                let rc = car.upgrade()?;
                let atom = rc.as_ref().borrow();
                match *atom {
                    Atom::Reserved(Reserved::Quote) => self
                        .push(Reg::Code, Op::Press(Reg::Stack))?
                        .push(Reg::Code, Op::Head(Reg::Stack))?
                        .push(Reg::Code, Op::Pop(Reg::Stack))?
                        .push(Reg::Code, Op::Head(Reg::Stack)),
                    Atom::Reserved(
                        Reserved::Quasiquote | Reserved::Unquote,
                    ) => self.eval_cons(&stack_atom),
                    Atom::Reserved(Reserved::UnqSpl) => self
                        .push(Reg::Code, Op::Splice)?
                        .eval_cons(&stack_atom),
                    _ => {
                        if self.qqlv < 1usize {
                            self.eval_cons(&stack_atom)
                        } else {
                            self.qq_cons(&stack_atom)
                        }
                    }
                }
            }
        }
    }
    // ========================================================================
    /// fn `call_unq`
    fn call_unq(&mut self, weak: WeakAtom) -> Result<&mut Self> {
        #[cfg(debug_assertions)]
        log::debug!("{}: call_unq", line!());
        if self.qqlv < 1usize {
            return Err(Error::InvalidQqlv);
        } else if 1usize != self.qqlv {
            drop(
                self.push(Reg::Code, Op::List(Reg::Stack))?
                    .push(Reg::Code, Op::Push(Reg::Stack, weak)),
            );
        }
        self.push(Reg::Code, Op::QqlvInc)?
            .push(Reg::Code, Op::Eval)? // not EvalRest
            .push(Reg::Code, Op::QqlvDec)?
            .push(Reg::Code, Op::Unlist(Reg::Stack))?
            .push(Reg::Code, Op::Pop(Reg::Stack))
    }
    // ------------------------------------------------------------------------
    /// fn call
    fn call(&mut self) -> Result<&mut Self> {
        #[cfg(debug_assertions)]
        log::debug!("{}: call", line!());
        let top_weak = self.top(Reg::Stack)?;
        let top_rc = top_weak.upgrade()?;
        let top_atom = top_rc.as_ref().borrow();
        match *top_atom {
            Atom::Op(ref x) => self
                .push(Reg::Code, x.clone())?
                .push(Reg::Code, Op::EvalRest)?
                .push(Reg::Code, Op::Unlist(Reg::Stack))?
                .push(Reg::Code, Op::Pop(Reg::Stack)),
            Atom::Reserved(Reserved::Quasiquote) => {
                if 0usize != self.qqlv {
                    let _ = self
                        .push(Reg::Code, Op::List(Reg::Stack))?
                        .push(Reg::Code, Op::Push(Reg::Stack, top_weak))?;
                }
                self.push(Reg::Code, Op::QqlvDec)?
                    .push(Reg::Code, Op::Eval)? // not EvalRest
                    .push(Reg::Code, Op::QqlvInc)?
                    .push(Reg::Code, Op::Unlist(Reg::Stack))?
                    .push(Reg::Code, Op::Pop(Reg::Stack))
            }
            Atom::Reserved(Reserved::Unquote | Reserved::UnqSpl) => {
                self.call_unq(top_weak)
            }
            Atom::Reserved(Reserved::Lambda) => self
                .push(Reg::Code, Op::Reserved(Reserved::Lambda))?
                .push(Reg::Code, Op::Pop(Reg::Stack)),
            Atom::Reserved(
                x @ (Reserved::Define
                | Reserved::Setx
                | Reserved::Cond
                | Reserved::If),
            ) => self
                .push(Reg::Code, Op::Reserved(x))?
                .push(Reg::Code, Op::Unlist(Reg::Stack))?
                .push(Reg::Code, Op::Pop(Reg::Stack)),
            Atom::Procedure { .. } => self
                .push(Reg::Code, Op::Apply)?
                .push(Reg::Code, Op::MoveTop(Reg::Work, Reg::Stack))?
                .push(Reg::Code, Op::EvalRest)?
                .push(Reg::Code, Op::Unlist(Reg::Stack))?
                .push(Reg::Code, Op::MoveTop(Reg::Stack, Reg::Work)),
            ref x => Err(Error::NotCallable(format!("{x}"))),
        }
    }
    // ========================================================================
    fn apply_bind(&mut self, formals_: WeakAtom) -> Result<&mut Self> {
        #[cfg(debug_assertions)]
        log::debug!("{}: apply_bind", line!());
        let _ = self.push(Reg::Env, Atom::Nil)?;
        let mut formals = formals_;
        loop {
            let formals_rc = formals.upgrade()?;
            let formals_atom = formals_rc.as_ref().borrow();
            match *formals_atom {
                Atom::Nil => {
                    break;
                }
                Atom::Symbol(_) => {
                    if self.reg_get(Reg::Stack).is(AtomClass::Nil)? {
                        return Err(Error::InvalidArg(line!()));
                    }
                    let _ = self
                        .list(Reg::Stack)?
                        .push(Reg::Stack, formals)?
                        .define_impl()?;
                    break;
                }
                Atom::Cons {
                    ref car, ref cdr, ..
                } => {
                    if self.reg_get(Reg::Stack).is(AtomClass::Nil)? {
                        return Err(Error::InvalidArg(line!()));
                    }
                    let _ =
                        self.push(Reg::Stack, car.clone())?.define_impl()?;
                    formals = cdr.clone();
                }
                _ => {
                    return Err(Error::InvalidFormals(
                        line!(),
                        format!("{formals}"),
                    ));
                }
            }
        }
        Ok(self)
    }
    // ------------------------------------------------------------------------
    fn apply(&mut self) -> Result<&mut Self> {
        #[cfg(debug_assertions)]
        log::debug!("{}: apply", line!());
        let proc_rc = self.top(Reg::Stack)?.upgrade()?;
        let _ = self.pop(Reg::Stack)?;
        let proc_atom = &*proc_rc.as_ref().borrow();
        match *proc_atom {
            Atom::Procedure {
                ref env,
                ref formals,
                ref body,
            } => {
                self.reg_set(Reg::Env, env.clone());
                let formals_rc = formals.upgrade()?;
                let formals_atom = formals_rc.as_ref().borrow();
                match *formals_atom {
                    Atom::Nil => {}
                    Atom::Symbol(_) | Atom::Cons { .. } => {
                        let _ = self.apply_bind(formals.clone())?;
                    }
                    _ => {
                        return Err(Error::InvalidFormals(
                            line!(),
                            format!("{formals}"),
                        ));
                    }
                }
                self.reg_set(Reg::Stack, body.clone());
                let _ = self.begin()?;
                Ok(self)
            }
            ref x => Err(Error::NotApplicable(format!("{x}"))),
        }
    }
    // ========================================================================
    fn begin(&mut self) -> Result<SECDMixed<'_>> {
        #[cfg(debug_assertions)]
        log::debug!("{}: begin", line!());
        let stack_rc = self.reg_get(Reg::Stack).upgrade()?;
        let stack_atom = stack_rc.as_ref().borrow();
        match *stack_atom {
            Atom::Nil => Ok(self.mixed()),
            Atom::Cons { ref cdr, .. } => {
                let cdr_rc = cdr.upgrade()?;
                let cdr_atom = cdr_rc.as_ref().borrow();
                if cdr_atom.is(AtomClass::Nil) {
                    self.mixed().push_code(Op::Eval)
                } else {
                    self.mixed()
                        .push_code(OpMixed::Begin)?
                        .push_code(Op::Pop(Reg::Stack))?
                        .push_code(Op::Eval)
                }
            }
            _ => Err(Error::NotCons(file!().to_string(), line!())),
        }
    }
    // ========================================================================
    fn cmp(
        &mut self,
        func: impl Fn(&Atom, &Atom) -> Result<bool>,
    ) -> Result<&mut Self> {
        #[cfg(debug_assertions)]
        log::debug!("{}: cmp", line!());
        let x = {
            let lhs_cell_rc = self.reg_get(Reg::Stack).upgrade()?;
            let lhs_cell_atom = lhs_cell_rc.as_ref().borrow();
            match *lhs_cell_atom {
                Atom::Cons {
                    car: ref lhs_weak,
                    cdr: ref rhs_cell_weak,
                } => {
                    let rhs_cell_rc = rhs_cell_weak.upgrade()?;
                    let rhs_cell_atom = rhs_cell_rc.as_ref().borrow();
                    match *rhs_cell_atom {
                        Atom::Cons {
                            car: ref rhs_weak, ..
                        } => {
                            let lhs_rc = lhs_weak.upgrade()?;
                            let lhs = &*lhs_rc.as_ref().borrow();
                            let rhs_rc = rhs_weak.upgrade()?;
                            let rhs = &*rhs_rc.as_ref().borrow();
                            func(lhs, rhs)?
                        }
                        _ => {
                            return Err(Error::NotCons(
                                file!().to_string(),
                                line!(),
                            ));
                        }
                    }
                }
                _ => {
                    return Err(Error::NotCons(file!().to_string(), line!()));
                }
            }
        };
        self.pop(Reg::Stack)?.pop(Reg::Stack)?.push(Reg::Stack, x)
    }
    // ------------------------------------------------------------------------
    fn cmp_num(
        &mut self,
        func: impl Fn(&Fraction, &Fraction) -> Result<bool>,
    ) -> Result<&mut Self> {
        #[cfg(debug_assertions)]
        log::debug!("{}: cmp_num", line!());
        let top_rc = self.top(Reg::Stack)?.upgrade()?;
        let top_atom = top_rc.as_ref().borrow();
        match *top_atom {
            Atom::Number(ref x) => self
                .pop(Reg::Stack)?
                .push(Reg::Stack, *x)? // clone to prevent overwriting
                .cmp(|lhs, rhs| {
                    if let Atom::Number(l) = lhs {
                        if let Atom::Number(r) = rhs {
                            func(l, r)
                        } else {
                            Err(Error::InvalidArg(line!()))
                        }
                    } else {
                        Err(Error::InvalidArg(line!()))
                    }
                }),
            _ => Err(Error::NotCons(file!().to_string(), line!())),
        }
    }
    // ------------------------------------------------------------------------
    fn eq(&mut self) -> Result<&mut Self> {
        #[cfg(debug_assertions)]
        log::debug!("{}: eq", line!());
        self.cmp_num(|lhs, rhs| Ok(lhs == rhs))
    }
    // ------------------------------------------------------------------------
    fn ne(&mut self) -> Result<&mut Self> {
        #[cfg(debug_assertions)]
        log::debug!("{}: ne", line!());
        self.cmp_num(|lhs, rhs| Ok(lhs != rhs))
    }
    // ------------------------------------------------------------------------
    fn lt(&mut self) -> Result<&mut Self> {
        #[cfg(debug_assertions)]
        log::debug!("{}: lt", line!());
        self.cmp_num(|lhs, rhs| Ok(lhs < rhs))
    }
    // ------------------------------------------------------------------------
    fn gt(&mut self) -> Result<&mut Self> {
        #[cfg(debug_assertions)]
        log::debug!("{}: gt", line!());
        self.cmp_num(|lhs, rhs| Ok(lhs > rhs))
    }
    // ------------------------------------------------------------------------
    fn le(&mut self) -> Result<&mut Self> {
        #[cfg(debug_assertions)]
        log::debug!("{}: le", line!());
        self.cmp_num(|lhs, rhs| Ok(lhs <= rhs))
    }
    // ------------------------------------------------------------------------
    fn ge(&mut self) -> Result<&mut Self> {
        #[cfg(debug_assertions)]
        log::debug!("{}: ge", line!());
        self.cmp_num(|lhs, rhs| Ok(lhs >= rhs))
    }
    // ========================================================================
    fn foldl(
        &mut self,
        func: impl Fn(&mut Atom, &Atom) -> Result<()>,
    ) -> Result<&mut Self> {
        #[cfg(debug_assertions)]
        log::debug!("{}: foldl", line!());
        loop {
            {
                let lhs_cell_rc = self.reg_get(Reg::Stack).upgrade()?;
                let lhs_cell_atom = lhs_cell_rc.as_ref().borrow();
                match *lhs_cell_atom {
                    Atom::Cons {
                        car: ref lhs_weak,
                        cdr: ref rhs_cell_weak,
                    } => {
                        let rhs_cell_rc = rhs_cell_weak.upgrade()?;
                        let rhs_cell_atom = rhs_cell_rc.as_ref().borrow();
                        match *rhs_cell_atom {
                            Atom::Nil => break,
                            Atom::Cons {
                                car: ref rhs_weak, ..
                            } => {
                                let lhs_rc = lhs_weak.upgrade()?;
                                let lhs = &mut *lhs_rc.as_ref().borrow_mut();
                                let rhs_rc = rhs_weak.upgrade()?;
                                let rhs = &*rhs_rc.as_ref().borrow();
                                func(lhs, rhs)?;
                            }
                            _ => {
                                return Err(Error::NotCons(
                                    file!().to_string(),
                                    line!(),
                                ));
                            }
                        }
                    }
                    _ => {
                        return Err(Error::NotCons(
                            file!().to_string(),
                            line!(),
                        ));
                    }
                }
            }
            let _ = self.press(Reg::Stack)?;
        }
        Ok(self)
    }
    // ========================================================================
    fn arithmetic(
        &mut self,
        func: impl Fn(&mut Fraction, &Fraction) -> Result<()>,
    ) -> Result<&mut Self> {
        #[cfg(debug_assertions)]
        log::debug!("{}: arithmetic", line!());
        let top_rc = self.top(Reg::Stack)?.upgrade()?;
        let top_atom = top_rc.as_ref().borrow();
        match *top_atom {
            Atom::Number(ref x) => self
                .pop(Reg::Stack)?
                .push(Reg::Stack, *x)? // clone to prevent overwriting
                .foldl(|lhs, rhs| {
                    if let Atom::Number(l) = lhs {
                        if let Atom::Number(r) = rhs {
                            func(l, r)
                        } else {
                            Err(Error::InvalidArg(line!()))
                        }
                    } else {
                        Err(Error::InvalidArg(line!()))
                    }
                }),
            _ => Err(Error::NotCons(file!().to_string(), line!())),
        }
    }
    // ------------------------------------------------------------------------
    /// fn mul
    fn mul(&mut self) -> Result<&mut Self> {
        #[cfg(debug_assertions)]
        log::debug!("{}: mul", line!());
        self.arithmetic(|lhs, rhs| {
            *lhs *= *rhs;
            Ok(())
        })
    }
    // ------------------------------------------------------------------------
    /// fn div
    fn div(&mut self) -> Result<&mut Self> {
        #[cfg(debug_assertions)]
        log::debug!("{}: div", line!());
        self.arithmetic(|lhs, rhs| {
            *lhs /= *rhs;
            Ok(())
        })
    }
    // ------------------------------------------------------------------------
    /// fn add
    fn add(&mut self) -> Result<&mut Self> {
        #[cfg(debug_assertions)]
        log::debug!("{}: add", line!());
        self.arithmetic(|lhs, rhs| {
            *lhs += *rhs;
            Ok(())
        })
    }
    // ------------------------------------------------------------------------
    /// fn sub
    fn sub(&mut self) -> Result<&mut Self> {
        #[cfg(debug_assertions)]
        log::debug!("{}: sub", line!());
        self.arithmetic(|lhs, rhs| {
            *lhs -= *rhs;
            Ok(())
        })
    }
    // ========================================================================
    /// fn run
    pub(crate) fn run(&mut self) -> Result<&mut Self> {
        #[cfg(debug_assertions)]
        log::debug!("{}: run", line!());
        loop {
            let code_rc = self.top(Reg::Code)?.upgrade()?;
            let _ = self.pop(Reg::Code)?;
            let code_atom = code_rc.as_ref().borrow();
            match *code_atom {
                Atom::Nil => break,
                Atom::Op(ref x) => {
                    let _ = self.operate(x)?;
                }
                ref x => {
                    return Err(Error::InvalidCode(format!("{x}")));
                }
            }
        }
        Ok(self)
    }
    // ------------------------------------------------------------------------
    /// fn operate
    #[allow(clippy::too_many_lines)]
    fn operate(&mut self, op: &Op) -> Result<&mut Self> {
        #[cfg(debug_assertions)]
        log::debug!("{}: operate", line!());

        // TODO(hanepjiv): emit

        match *op {
            Op::Push(reg, ref weak) => self.push(reg, weak.clone()),
            Op::Pop(reg) => self.pop(reg),
            Op::Press(reg) => self.press(reg),
            Op::Swap(reg) => self.swap(reg),
            Op::Head(reg) => self.head(reg),
            Op::Cons(reg) => self.cons(reg),
            Op::XCons(reg) => self.xcons(reg),
            Op::Car(reg) => self.car(reg),
            Op::Cdr(reg) => self.cdr(reg),
            Op::List(reg) => self.list(reg),
            Op::Unlist(reg) => self.unlist(reg),
            Op::MoveTop(from, to) => self.move_top(from, to),
            // ----------------------------------------------------------------
            Op::Jump(n) => {
                for _ in 0usize..n {
                    let _ = self.pop(Reg::Code)?;
                }
                Ok(self)
            }
            // ----------------------------------------------------------------
            Op::JumpNil(n) => {
                let top_rc = self.top(Reg::Stack)?.upgrade()?;
                let top_atom = top_rc.as_ref().borrow();
                if matches!(*top_atom, Atom::Nil) {
                    for _ in 0usize..n {
                        let _ = self.pop(Reg::Code)?;
                    }
                }
                Ok(self)
            }
            // ----------------------------------------------------------------
            Op::JumpFalse(n) => {
                let top_rc = self.top(Reg::Stack)?.upgrade()?;
                let top_atom = top_rc.as_ref().borrow();
                match *top_atom {
                    Atom::Bool(x) => {
                        if !x {
                            for _ in 0usize..n {
                                let _ = self.pop(Reg::Code)?;
                            }
                        }
                        Ok(self)
                    }
                    Atom::Reserved(Reserved::Else) => Ok(self),
                    _ => Err(Error::InvalidArg(line!())),
                }
            }
            // ----------------------------------------------------------------
            Op::Print => {
                let _ = self.print();
                Ok(self)
            }
            // ----------------------------------------------------------------
            Op::Display => {
                drop(self.display());
                Ok(self)
            }
            Op::DisplayLn => {
                drop(self.displayln());
                Ok(self)
            }
            Op::NewLine => {
                let _ = self.newline();
                Ok(self)
            }
            // ----------------------------------------------------------------
            Op::Find => self.find(),
            Op::DefineImpl => self.define_impl(),
            Op::SetxImpl => self.setx_impl(),
            // ----------------------------------------------------------------
            Op::DumpSave(stack, code) => self.dump_save(stack, code),
            Op::DumpLoad => self.dump_load(),
            Op::Splice => self.splice(),
            Op::Eval => self.eval(),
            Op::EvalRest => self.eval_rest(),
            Op::QqlvInc => Ok(self.qqlv_inc()),
            Op::QqlvDec => Ok(self.qqlv_dec()),
            Op::Call => self.call(),
            Op::Apply => self.apply(),
            // ----------------------------------------------------------------
            Op::EQ => self.eq(),
            Op::NE => self.ne(),
            Op::LT => self.lt(),
            Op::GT => self.gt(),
            Op::LE => self.le(),
            Op::GE => self.ge(),
            // ----------------------------------------------------------------
            Op::Mul => self.mul(),
            Op::Div => self.div(),
            Op::Add => self.add(),
            Op::Sub => self.sub(),
            // ----------------------------------------------------------------
            Op::Reserved(x) => match x {
                Reserved::Lambda => self.lambda(),
                Reserved::Define => Ok(self.define()?.into()),
                Reserved::Setx => self.setx(),
                Reserved::Cond => self.cond(),
                Reserved::If => self.if_(),
                _ => unimplemented!("Op::Reserved({})", x),
            },
            // ----------------------------------------------------------------
            Op::Mixed(x) => {
                let _count = match x {
                    OpMixed::Begin => self.begin()?.count(),
                    OpMixed::Eval => unimplemented!("Op::Mixed({})", x),
                };
                // Todo(hanepjiv): checks for count
                Ok(self)
            }
        }
    }
    // ========================================================================
    /// fn mixed
    pub(crate) const fn mixed(&mut self) -> SECDMixed<'_> {
        SECDMixed::new(self)
    }
}
// ////////////////////////////////////////////////////////////////////////////
// ============================================================================
#[doc(hidden)]
#[derive(Debug)]
pub(crate) struct SECDMixed<'a> {
    secd: &'a mut SECD,
    cnt: usize,
}
// ============================================================================
impl<'a> SECDMixed<'a> {
    // ========================================================================
    /// fn new
    pub(crate) const fn new(secd: &'a mut SECD) -> Self {
        SECDMixed { secd, cnt: 0usize }
    }
    // ========================================================================
    /// fn `push_code`
    pub(crate) fn push_code(mut self, x: impl Into<Op>) -> Result<Self> {
        #[cfg(debug_assertions)]
        log::debug!("{}: push_code", line!());
        let _ = self.secd.push(Reg::Code, x.into())?;
        self.cnt += 1usize;
        Ok(self)
    }
    // ========================================================================
    /// fn count
    pub(crate) const fn count(self) -> usize {
        self.cnt
    }
}
// ============================================================================
impl<'a> From<SECDMixed<'a>> for &'a mut SECD {
    fn from(x: SECDMixed<'a>) -> Self {
        x.secd
    }
}
