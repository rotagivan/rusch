// -*- mode:rust; coding:utf-8-unix; -*-

//! register.rs

//  Copyright 2017 hanepjiv
//  @author hanepjiv <hanepjiv@gmail.com>
//  @copyright The MIT License (MIT) / Apache License Version 2.0
//  @since 2018/07/06
//  @date 2025/03/08

// ////////////////////////////////////////////////////////////////////////////
// use  =======================================================================
// ////////////////////////////////////////////////////////////////////////////
// ============================================================================
/// enum Reg
#[derive(Debug, Clone, Copy, PartialOrd, Ord, PartialEq, Eq, Hash)]
pub(crate) enum Reg {
    /// Stack
    Stack,
    /// Env
    Env,
    /// Code
    Code,
    /// Dump
    Dump,
    /// Work
    Work,
    /// Nil
    #[allow(dead_code)]
    Nil,
}
// ============================================================================
impl std::fmt::Display for Reg {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        <Self as std::fmt::Debug>::fmt(self, f)
    }
}
// ////////////////////////////////////////////////////////////////////////////
// ============================================================================
#[cfg(test)]
mod tests {
    // use  ===================================================================
    use super::*;
    // ========================================================================
    #[test]
    const fn test_send() {
        const fn assert_send<T: Send>() {}
        assert_send::<Reg>();
    }
    // ------------------------------------------------------------------------
    #[test]
    const fn test_sync() {
        const fn assert_sync<T: Sync>() {}
        assert_sync::<Reg>();
    }
}
