// -* -mode:rust;coding:utf-8-unix; -*-

//! main.rs

//  Copyright 2017 hanepjiv
//  @author hanepjiv <hanepjiv@gmail.com>
//  @copyright The MIT License (MIT) / Apache License Version 2.0
//  @since 2018/07/05
//  @date 2025/03/09

// ////////////////////////////////////////////////////////////////////////////
// use  =======================================================================
use std::{env, io::Write, path::Path};
// ----------------------------------------------------------------------------
use log::info;
// ----------------------------------------------------------------------------
use fraction as _;
#[cfg(feature = "realize")]
use serde as _;
#[cfg(feature = "realize")]
use serde_derive as _;
// ----------------------------------------------------------------------------
use rusch::RuSch;
// ////////////////////////////////////////////////////////////////////////////
// ============================================================================
type Result<T> = std::result::Result<T, rusch::Error>;
// ============================================================================
fn print_usage(opts: &getopts::Options) -> Result<()> {
    let _ = std::io::stdout().write(
        opts.usage(&format!(
            r"Usage:
    {0} [FilePath]

FilePath:               file path to load",
            rusch::MODULE_NAME,
        ))
        .as_bytes(),
    )?;
    Ok(())
}
// ============================================================================
fn app() -> Result<()> {
    env_logger::init();

    info!("{:?}", rusch::MODULE_VERSTR);

    let mut opts = getopts::Options::new();
    let _ = opts.optflag("h", "help", "show this message").optflag(
        "v",
        "version",
        "show version",
    );

    let matches = opts.parse(env::args().skip(1))?;
    if matches.opt_present("v") {
        let stdout = std::io::stdout();
        let mut handle = stdout.lock();
        let _ = handle.write(rusch::MODULE_VERSTR.as_bytes())?;
        let _ = handle.write(b"\n")?;
        return Ok(());
    }
    if matches.opt_present("h") || matches.free.is_empty() {
        return print_usage(&opts);
    }

    let _ = RuSch::new()?.load_file(Path::new(&matches.free[0]))?;

    Ok(())
}
// ============================================================================
fn main() -> Result<()> {
    if let Err(x) = app() {
        let ln = b"\n";

        let stderr = std::io::stderr();
        let mut handle = stderr.lock();

        let _ = handle.write(rusch::MODULE_VERSTR.as_bytes())?;
        let _ = handle.write(ln)?;

        let _ = handle.write(x.to_string().as_bytes())?;
        let _ = handle.write(ln)?;
        /*
        for c in x.causes() {
            let _ = handle.write(c.to_string().as_bytes())?;
            let _ = handle.write(ln)?;
        }
         */
    }
    Ok(())
}
