// -*- mode:rust; coding:utf-8-unix; -*-

//! rusch.rs

//  Copyright 2017 hanepjiv
//  @author hanepjiv <hanepjiv@gmail.com>
//  @copyright The MIT License (MIT) / Apache License Version 2.0
//  @since 2018/07/06
//  @date 2025/03/09

// ////////////////////////////////////////////////////////////////////////////
#![allow(unused_crate_dependencies, missing_docs)]
// use  =======================================================================
use criterion::{Criterion, criterion_group, criterion_main};
use rusch::RuSch;
// ////////////////////////////////////////////////////////////////////////////
// ============================================================================
fn bench_rusch(c: &mut Criterion) {
    let _ = c.bench_function("bench_rusch", |b| {
        b.iter(|| RuSch::new().unwrap());
    });
}
// ============================================================================
fn bench_rusch_entry(c: &mut Criterion) {
    let _ = c.bench_function("bench_rusch_entry", |b| {
        b.iter(|| {
            let mut rusch = RuSch::new().unwrap();
            let _ = rusch.entry().unwrap();
        });
    });
}
// ============================================================================
criterion_group!(benches, bench_rusch, bench_rusch_entry);
criterion_main!(benches);
